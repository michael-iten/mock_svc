package ch.axonivy.fintech.finnova.service.clientoverview;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class PrivateIndividualService extends AbstractFinnovaService {

	private static final String PRIVATE_INDIVIDUAL = "privateIndividual";
	private static final String CLIENT_KEY = "clientKey";

	public static PrivateIndividualService createInstance() {
		return new PrivateIndividualService();
	}

	public String getFinovaData(String clientKeyParam) {
		return getFinnovaData(FinnovaConstantFile.PRIVATE_INDIVIDUAL_FILE_NAME, CLIENT_KEY, clientKeyParam,
				PRIVATE_INDIVIDUAL, false);
	}
}
