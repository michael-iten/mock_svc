package ch.axonivy.fintech.finnova.service.clientoverview;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class ClientStatusService extends AbstractFinnovaService{
	public static ClientStatusService createInstance() {
		return new ClientStatusService();
	}

	public String getClientStatuses() {
		return readFile(FinnovaConstantFile.CLIENT_STATUSES_FILE_NAME);
	}
}
