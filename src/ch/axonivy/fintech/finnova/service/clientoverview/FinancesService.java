package ch.axonivy.fintech.finnova.service.clientoverview;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class FinancesService extends AbstractFinnovaService {

	private static final String CLIENT_KEY = "clientKey";
	private static final String FINANCES = "finances";

	public static FinancesService createInstance() {
		return new FinancesService();
	}

	public String getFinances(String clientKeyParam) {
		return getFinnovaData(FinnovaConstantFile.FINANCES_FILE_NAME, CLIENT_KEY, clientKeyParam, FINANCES, false);
	}
}
