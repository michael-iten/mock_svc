package ch.axonivy.fintech.finnova.service.clientoverview;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class ClientMainTypeService extends AbstractFinnovaService {

	private static final String CLIENT_KEY = "clientKey";
	private static final String CLIENT_MAIN_TYPE = "clientMainType";

	public static ClientMainTypeService createInstance() {
		return new ClientMainTypeService();
	}

	public String getFinnovaData(String clientKeyParam) {
		return getFinnovaData(FinnovaConstantFile.CLIENT_MAIN_TYPE_FILE_NAME, CLIENT_KEY, clientKeyParam,
				CLIENT_MAIN_TYPE, false);
	}

}
