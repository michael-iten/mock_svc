package ch.axonivy.fintech.finnova.service.clientoverview;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class TwoPersonHouseholdService extends AbstractFinnovaService {

	private static final String CLIENT_KEY = "clientKey";
	private static final String TWO_PERSON_HOUSE_HOLD = "twoPersonHousehold";

	public static TwoPersonHouseholdService createInstance() {
		return new TwoPersonHouseholdService();
	}

	public String getFinovaData(String clientKeyParam) {
		return getFinnovaData(FinnovaConstantFile.TWO_PERSON_HOUSE_GOLD_FILE_NAME, CLIENT_KEY, clientKeyParam,
				TWO_PERSON_HOUSE_HOLD, false);
	}
}
