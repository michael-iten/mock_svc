package ch.axonivy.fintech.finnova.service.loanadvisory;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class PricingService extends AbstractFinnovaService{
	
	private static final String ORDER_KEY = "orderKey";
	private static final String PRICINGS = "pricings";
	
	public static PricingService createInstance(){
		return new PricingService();
	}

	public String getPricings(String orderKeyParam) {
		return getFinnovaData(FinnovaConstantFile.PRICINGS_FILE_NAME, ORDER_KEY, orderKeyParam, PRICINGS, true);
	}
}
