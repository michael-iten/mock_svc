package ch.axonivy.fintech.finnova.service.loanadvisory;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class ProductTariffsService extends AbstractFinnovaService{
	
	public static ProductTariffsService createInstance() {
		return new ProductTariffsService();
	}

	public String getProductTariffs() {
		return readFile(FinnovaConstantFile.PRODUCT_TARIFFS_FILE_NAME);
	}
}
