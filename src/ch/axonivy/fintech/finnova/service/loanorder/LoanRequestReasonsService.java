package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class LoanRequestReasonsService extends AbstractFinnovaService {
	
	public static LoanRequestReasonsService createInstance() {
		return new LoanRequestReasonsService();
	}

	public String getLoanRequestReasons() {
		return readFile(FinnovaConstantFile.LOAN_REQUEST_REASONS_FILE_NAME);
	}
}
