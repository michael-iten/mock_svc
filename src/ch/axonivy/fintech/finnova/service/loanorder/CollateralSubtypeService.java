package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class CollateralSubtypeService extends AbstractFinnovaService {

	public static CollateralSubtypeService createInstance() {
		return new CollateralSubtypeService();
	}
	
	public String getCollateralSubtypes() {
		return readFile(FinnovaConstantFile.COLLATERAL_SUBTYPES_FILE_NAME);
	}
}
