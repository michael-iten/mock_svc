package ch.axonivy.fintech.finnova.service.loanorder;

import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;
import ch.axonivy.fintech.finnova.service.util.JsonUtil;

public class OrderService extends AbstractFinnovaService {

	private static final String ORDER_DETAIL = "orderDetail";
	private static final String ORDER_KEY = "orderKey";
	private static final String ORDERS = "orders";
	private static final String CLIENT_KEY = "clientKey";
	
	public static OrderService createInstance() {
		return new OrderService();
	}

	public String getClientOrdersData(String clientKeyParam) {
		return getFinnovaData(FinnovaConstantFile.CLIENT_ORDER_FILE_NAME, CLIENT_KEY, clientKeyParam, ORDERS, true);
	}
	
	public String getClientOrderDetail(String orderKeyParam) {
		String orderDetailFileContent = readFile(FinnovaConstantFile.CLIENT_ORDER_DETAIL_FILE_NAME);
		JsonArray orderDetailJsonArray = JsonUtil.getJsonArrayFromString(orderDetailFileContent);

		Iterator<JsonElement> iterator = orderDetailJsonArray.iterator();

		while (iterator.hasNext()) {
			JsonElement order = iterator.next();
			String orderKey = order.getAsJsonObject().get(ORDER_KEY).getAsString();
			if (orderKey.equalsIgnoreCase(orderKeyParam)) {
				return order.getAsJsonObject().get(ORDER_DETAIL).getAsJsonObject().toString();
			}
		}
		return readFile(FinnovaConstantFile.CLIENT_ORDER_DETAIL_EMPTY_FILE_NAME);
	}
}
