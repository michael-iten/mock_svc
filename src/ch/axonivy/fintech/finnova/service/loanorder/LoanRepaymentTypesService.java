package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class LoanRepaymentTypesService extends AbstractFinnovaService {
	
	public static LoanRepaymentTypesService createInstance() {
		return new LoanRepaymentTypesService();
	}

	public String getLoanRepaymentTypes() {
		return readFile(FinnovaConstantFile.LOAN_REPAYMENT_TYPES_FILE_NAME);
	}
}
