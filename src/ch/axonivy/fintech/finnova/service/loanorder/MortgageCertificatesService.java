package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class MortgageCertificatesService extends AbstractFinnovaService {

	private static final String ORDER_KEY = "orderKey";
	private static final String MORTGAGE_CERTIFICATES = "mortgagecertificates";
	
	public static MortgageCertificatesService createInstance() {
		return new MortgageCertificatesService();
	}
	
	public String getMortgageCertificatesByOrderKey(String orderKey) {
		return getFinnovaData(FinnovaConstantFile.MORTGAGE_CERTIFICATES_FILE_NAMWE, ORDER_KEY, orderKey,
				MORTGAGE_CERTIFICATES, true);
	}
}
