package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class AmortisationService extends AbstractFinnovaService {
	
	private static final String AMORTISATIONS = "amortisations";
	private static final String ORDER_KEY = "orderKey";
	
	public static AmortisationService createInstance() {
		return new AmortisationService();
	}
	
	public String get(String orderKey) {
		return getFinnovaData(FinnovaConstantFile.CLIENT_ORDER_AMORTISATION_FILE_NAME, ORDER_KEY, orderKey,
				AMORTISATIONS, true);
	}
	
}
