package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class SecurityService extends AbstractFinnovaService {

	private static final String ORDER_KEY = "orderKeyOnlyForFinnova";
	private static final String SECURITIES = "securities";

	public static SecurityService createInstance() {
		return new SecurityService();
	}

	public String getSecuritiesByOrderKey(String orderKey) {
		return getFinnovaData(FinnovaConstantFile.SECURITIES_FILE_NAME, ORDER_KEY, orderKey, SECURITIES, true);
	}
}
