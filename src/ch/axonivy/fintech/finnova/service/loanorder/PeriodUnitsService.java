package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class PeriodUnitsService extends AbstractFinnovaService {
	
	public static PeriodUnitsService createInstance() {
		return new PeriodUnitsService();
	}

	public String getPeriodUnits() {
		return readFile(FinnovaConstantFile.PERIOD_UNITS_FILE_NAME);
	}
}