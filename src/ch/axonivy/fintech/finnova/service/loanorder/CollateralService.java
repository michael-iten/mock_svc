package ch.axonivy.fintech.finnova.service.loanorder;

import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;
import ch.axonivy.fintech.finnova.service.util.JsonUtil;

public class CollateralService extends AbstractFinnovaService {

	public static CollateralService createInstance() {
		return new CollateralService();
	}
	
	public String getClientOrderCollateralData(String orderKeyParam) {
		String content = readFile(FinnovaConstantFile.CLIENT_ORDER_COLLATERAL_FILE_NAME);
		JsonArray collaterals = JsonUtil.getJsonArrayFromString(content);
		JsonArray collateralResult = new JsonArray();
		
		Iterator<JsonElement> collateralIterator = collaterals.iterator();
		
		while (collateralIterator.hasNext()) {
			JsonElement collateral = collateralIterator.next();
			String orderKeyOfCollateral = collateral.getAsJsonObject().get("orderKey").getAsString();
			if (orderKeyParam.equalsIgnoreCase(orderKeyOfCollateral)) {
				collateralResult.add(collateral);
			}
		}

		return collateralResult.toString();
	}
}
