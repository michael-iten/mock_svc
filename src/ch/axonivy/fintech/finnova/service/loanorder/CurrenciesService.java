package ch.axonivy.fintech.finnova.service.loanorder;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class CurrenciesService extends AbstractFinnovaService {
	
	public static CurrenciesService createInstance() {
		return new CurrenciesService();
	}

	public String getCurrencies() {
		return readFile(FinnovaConstantFile.CURRENCIES_FILE_NAME);
	}
}
