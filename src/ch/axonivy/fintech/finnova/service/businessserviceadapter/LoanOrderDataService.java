package ch.axonivy.fintech.finnova.service.businessserviceadapter;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class LoanOrderDataService extends AbstractFinnovaService {

	private static final String LOAN_ORDER_DATA = "loanOrderData";
	private static final String ORDER_KEY = "orderKey";

	public static LoanOrderDataService createInstance() {
		return new LoanOrderDataService();
	}

	public String getLoanOrderData(String orderKeyParam) {
		return getFinnovaData(FinnovaConstantFile.LOAN_ORDER_DATA_FILE_NAME, ORDER_KEY, orderKeyParam, LOAN_ORDER_DATA,
				true);
	}
}
