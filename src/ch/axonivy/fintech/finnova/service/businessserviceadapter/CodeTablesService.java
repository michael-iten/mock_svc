package ch.axonivy.fintech.finnova.service.businessserviceadapter;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class CodeTablesService extends AbstractFinnovaService {
	
	public static CodeTablesService createInstance() {
		return new CodeTablesService();
	}

	public String getCodeTables() {
		return readFile(FinnovaConstantFile.CODE_TABLES_FILE_NAME);
	}
}
