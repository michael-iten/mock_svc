package ch.axonivy.fintech.finnova.service.businessserviceadapter;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class AddressDataService extends AbstractFinnovaService {
	
	private static final String CLIENT_NUMBER = "clientNumber";

	public static AddressDataService createInstance() {
		return new AddressDataService();
	}

	public String getAddressData(String customerNumber, boolean isAllAddresses) {
		if (isAllAddresses) {
			return readFile(FinnovaConstantFile.ADDRESS_DATA_FILE_NAME);
		}
		return getFinnovaData(FinnovaConstantFile.ADDRESS_DATA_FILE_NAME, CLIENT_NUMBER, customerNumber).toString();
	}

}
