package ch.axonivy.fintech.finnova.service.businessserviceadapter;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class ClientDataService extends AbstractFinnovaService {
	
	private static final String CUSTOMER_NUBER = "customerNumber";

	public static ClientDataService createInstance() {
		return new ClientDataService();
	}

	public String getClientData(String customerNumber, boolean isAllClients) {
		if (isAllClients) {
			return readFile(FinnovaConstantFile.CLIENT_DATA_FILE_NAME);
		}
		return getFinnovaData(FinnovaConstantFile.CLIENT_DATA_FILE_NAME, CUSTOMER_NUBER, customerNumber).toString();
	}

}
