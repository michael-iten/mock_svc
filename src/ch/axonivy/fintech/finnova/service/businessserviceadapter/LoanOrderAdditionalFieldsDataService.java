package ch.axonivy.fintech.finnova.service.businessserviceadapter;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class LoanOrderAdditionalFieldsDataService extends AbstractFinnovaService {

	private static final String LOAN_ORDER_ADDITIONAL_FIELDS_DATA = "loanOrderAdditionalFieldsData";
	private static final String ORDER_KEY = "orderKey";

	public static LoanOrderAdditionalFieldsDataService createInstance() {
		return new LoanOrderAdditionalFieldsDataService();
	}
	
	public String getLoanOrderAdditionalFieldsData(String orderKeyParam) {
		return getFinnovaData(FinnovaConstantFile.LOAN_ORDER_ADDITIONAL_FIELDS_DATA_FILE_NAME, ORDER_KEY, orderKeyParam,
				LOAN_ORDER_ADDITIONAL_FIELDS_DATA, true);
	}
}
