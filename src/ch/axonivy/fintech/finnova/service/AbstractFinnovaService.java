package ch.axonivy.fintech.finnova.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ch.axonivy.fintech.finnova.service.util.JsonUtil;
import ch.ivyteam.ivy.environment.Ivy;

public abstract class AbstractFinnovaService {

	private static final String FINNOVA_FOLDER_FILE_PATH = "ch_axonivy_fintech_finnova_folder_file_path";
	
	private String getDirectoryPath() {
		return Ivy.var().get(FINNOVA_FOLDER_FILE_PATH);
	}
	
	protected String readFile(String fileName) {
		String jsonResult = "";
		File file = new File(getDirectoryPath() + File.separator + fileName);
		try {
			jsonResult = FileUtils.readFileToString(file, StandardCharsets.UTF_8.name());
		} catch (IOException e) {
			Ivy.log().error(e);
		}
		return jsonResult;
	}
	
	protected String getFinnovaData(String fileName, String fetchingKey, String keyParam, String objectName,
			boolean isArray) {
		String content = readFile(fileName);
		JsonArray finnovaDatas = JsonUtil.getJsonArrayFromString(content);

		Iterator<JsonElement> iterator = finnovaDatas.iterator();
		while (iterator.hasNext()) {
			JsonElement item = iterator.next();
			String itemData = item.getAsJsonObject().get(fetchingKey).getAsString();
			if (keyParam.equalsIgnoreCase(itemData)) {
				return item.getAsJsonObject().get(objectName).toString();
			}
		}
		if (isArray) {
			return new JsonArray().toString();
		}
		return (new JsonObject()).toString();
	}
	
	protected JsonArray getFinnovaData(String fileName, String fetchingKeyParam, String fetchingKeyParamValue) {
		String content = readFile(fileName);
		JsonArray finnovaDatas = JsonUtil.getJsonArrayFromString(content);
		JsonArray finnovaDatasResult = new JsonArray();
		
		Iterator<JsonElement> finnovaDatasIterator = finnovaDatas.iterator();
		while(finnovaDatasIterator.hasNext()){
			JsonElement nextElement = finnovaDatasIterator.next();
			String fectchingKeyValue = nextElement.getAsJsonObject().get(fetchingKeyParam).getAsString();
			if (fectchingKeyValue.equalsIgnoreCase(fetchingKeyParamValue)) {
				finnovaDatasResult.add(nextElement.getAsJsonObject());
			}
		}
		return finnovaDatasResult;
	}
	
}
