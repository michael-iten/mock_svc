package ch.axonivy.fintech.finnova.service.realestate;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class CountriesService extends AbstractFinnovaService{
	
	public static CountriesService createInstance() {
		return new CountriesService();
	}

	public String getCountries() {
		return readFile(FinnovaConstantFile.COUNTRIES_FILE_NAME);
	}
}
