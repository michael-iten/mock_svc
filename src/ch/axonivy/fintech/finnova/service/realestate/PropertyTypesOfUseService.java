package ch.axonivy.fintech.finnova.service.realestate;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class PropertyTypesOfUseService extends AbstractFinnovaService{

	public static PropertyTypesOfUseService createInstance() {
		return new PropertyTypesOfUseService();
	}

	public String getPropertyTypesOfUse() {
		return readFile(FinnovaConstantFile.PROPERTY_TYPES_OF_USE_FILE_NAME);
	}
}
