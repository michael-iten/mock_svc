package ch.axonivy.fintech.finnova.service.realestate;

import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;
import ch.axonivy.fintech.finnova.service.util.JsonUtil;

public class PropertyService extends AbstractFinnovaService{
	
	private static final String PROPERTY_KEY = "key";

	public static PropertyService createInstance() {
		return new PropertyService();
	}
	
	public String getProperty(String propertyKeyParam){
		String propertiesContent = readFile(FinnovaConstantFile.CLIENT_PROPERTY_FILE_NAME);
		JsonArray propertiesJsonArray = JsonUtil.getJsonArrayFromString(propertiesContent);
		
		Iterator<JsonElement> propertyIterator = propertiesJsonArray.iterator();
		
		while(propertyIterator.hasNext()){
			JsonElement property = propertyIterator.next();
			String propertyKey = property.getAsJsonObject().get(PROPERTY_KEY).getAsString();
			if(propertyKey.equalsIgnoreCase(propertyKeyParam)){
				return property.toString();
			}
		}
		return readFile(FinnovaConstantFile.CLIENT_PROPERTY_EMPTY_FILE_NAME);
	}
}
