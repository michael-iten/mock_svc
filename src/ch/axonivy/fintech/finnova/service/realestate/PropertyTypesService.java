package ch.axonivy.fintech.finnova.service.realestate;

import ch.axonivy.fintech.finnova.service.AbstractFinnovaService;
import ch.axonivy.fintech.finnova.service.util.FinnovaConstantFile;

public class PropertyTypesService extends AbstractFinnovaService{

	public static PropertyTypesService createInstance() {
		return new PropertyTypesService();
	}

	public String getPropertyTypes() {
		return readFile(FinnovaConstantFile.PROPERTY_TYPES_FILE_NAME);
	}
}
