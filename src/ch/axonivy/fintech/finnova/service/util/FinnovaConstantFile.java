package ch.axonivy.fintech.finnova.service.util;

public class FinnovaConstantFile {
	
	private FinnovaConstantFile() {}
	
	public static final String ADDRESS_DATA_FILE_NAME = "adressen.txt";
	public static final String CLIENT_DATA_FILE_NAME = "clients_blacked.txt";
	public static final String CODE_TABLES_FILE_NAME = "codeTables.txt";
	public static final String CLIENT_ORDER_AMORTISATION_FILE_NAME = "order_amortisation.txt";
	public static final String CLIENT_ORDER_COLLATERAL_FILE_NAME = "order_collateral.txt";
	public static final String MORTGAGE_CERTIFICATES_FILE_NAMWE = "order_mortgagecertificates.txt";
	public static final String CLIENT_ORDER_FILE_NAME = "orders.txt";
	public static final String CLIENT_ORDER_DETAIL_FILE_NAME = "orders_detail.txt";
	public static final String CLIENT_ORDER_DETAIL_EMPTY_FILE_NAME = "orders_detail_empty.txt";
	public static final String SECURITIES_FILE_NAME = "order_security.txt";
	public static final String CLIENT_PROPERTY_FILE_NAME = "properties.txt";
	public static final String CLIENT_PROPERTY_EMPTY_FILE_NAME = "property_empty.txt";
	public static final String LOAN_ORDER_DATA_FILE_NAME = "loanOrderData.txt";
	public static final String LOAN_ORDER_ADDITIONAL_FIELDS_DATA_FILE_NAME = "loanOrderAdditionalFieldsData.txt";
	
	public static final String CURRENCIES_FILE_NAME = "currencies.txt";
	public static final String PERIOD_UNITS_FILE_NAME = "periodUnits.txt";
	public static final String COLLATERAL_SUBTYPES_FILE_NAME = "collateralSubtype.txt";
	
	public static final String PROPERTY_TYPES_OF_USE_FILE_NAME = "propertyTypesOfUse.txt";
	public static final String PROPERTY_TYPES_FILE_NAME = "propertyTypes.txt";
	
	public static final String LOAN_REQUEST_REASONS_FILE_NAME = "loanRequestReasons.txt";
	public static final String COUNTRIES_FILE_NAME = "countries.txt";
	public static final String LOAN_REPAYMENT_TYPES_FILE_NAME = "loanRepaymentTypes.txt";
	
	public static final String FINANCES_FILE_NAME = "finances.txt";
	public static final String CLIENT_MAIN_TYPE_FILE_NAME = "clientMainType.txt";
	public static final String PRIVATE_INDIVIDUAL_FILE_NAME = "privateIndividual.txt";
	public static final String TWO_PERSON_HOUSE_GOLD_FILE_NAME = "twoPersonHousehold.txt";
	public static final String PRICINGS_FILE_NAME = "pricings.txt";
	public static final String PRODUCT_TARIFFS_FILE_NAME = "productTariffs.txt";
	public static final String CLIENT_STATUSES_FILE_NAME = "clientStatuses.txt";
}
