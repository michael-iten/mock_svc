package ch.axonivy.fintech.finnova.service.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonUtil {
	
	private JsonUtil(){}
	
	public static JsonArray getJsonArrayFromString(String content) {
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(content);
		return json.getAsJsonArray();
	}
}
