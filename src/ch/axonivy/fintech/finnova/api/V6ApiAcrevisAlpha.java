package ch.axonivy.fintech.finnova.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@PermitAll
@Path("v6/banks/{bankId : (6900|6920)}")
@Produces("application/json")
public class V6ApiAcrevisAlpha extends V6Api {
	private static final Map<String, IdReservationResult> reservedAccounts = new HashMap<>();
	
	@POST
	@Path("clients/{clientId}/accounts")
	@Override
	public Response reserveAccountNumber(@PathParam("clientId") String clientId, @PathParam("bankId") String bankId) {
		validateToken();
		Response response = super.reserveAccountNumber(clientId, bankId);
		IdReservationResult account = (IdReservationResult) response.getEntity();
		reservedAccounts.put(account.id, account);
		return response;
	}
	
	@PUT	
	@Path("clients/{clientId}/accounts/{accountId}")
	public Response createAccount(@PathParam("accountId") String accountId, Map<String, Object> body) {
		validateToken();
		if (!reservedAccounts.containsKey(accountId)) {
			return Response.status(Status.BAD_REQUEST).entity("{\"error\":\"ACCOUNT ID WAS NOT RESERVED\"}").build();
		}
		Object givenAccountNumber = body.get("externalNumber");
		if (!Objects.equals(reservedAccounts.get(accountId).number, givenAccountNumber)) {
			return Response.status(Status.BAD_REQUEST).entity(
					"{\"error\":\"GIVEN ACCOUNT NUMBER '" + body.get("externalNumber") + 
					"' DOES NOT MATCH ACTUALL ACCOUNT NUMBER '" + 
					reservedAccounts.get(accountId).number + "'\"}").build();
		}
		
		return Response.ok(body).build();
	}
	
	
}
