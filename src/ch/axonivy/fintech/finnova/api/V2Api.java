package ch.axonivy.fintech.finnova.api;

import java.util.Objects;

import javax.annotation.security.PermitAll;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang3.StringUtils;

import ch.axonivy.fintech.finnova.service.loanorder.AmortisationService;
import ch.axonivy.fintech.finnova.service.loanorder.CollateralService;
import ch.axonivy.fintech.finnova.service.loanorder.CollateralSubtypeService;
import ch.axonivy.fintech.finnova.service.loanorder.CurrenciesService;
import ch.axonivy.fintech.finnova.service.loanorder.LoanRepaymentTypesService;
import ch.axonivy.fintech.finnova.service.loanorder.LoanRequestReasonsService;
import ch.axonivy.fintech.finnova.service.loanorder.MortgageCertificatesService;
import ch.axonivy.fintech.finnova.service.loanorder.OrderService;
import ch.axonivy.fintech.finnova.service.loanorder.PeriodUnitsService;
import ch.axonivy.fintech.finnova.service.loanorder.SecurityService;
import ch.ivyteam.ivy.environment.Ivy;

@Singleton
@Path("v2")
@PermitAll
public class V2Api {
	
	@GET
	@Path("/banks/{userbank}/orders/")
	@Produces({ "application/json" })
	public Response getOrders(@PathParam("userbank") Integer userbank,
			@QueryParam("clientKey") String clientKey,
			@Context ContainerRequestContext context) {
		Ivy.log().info("#####getOrders with Authorization access token: {0}", context.getHeaderString("Authorization"));
		if (StringUtils.isEmpty(clientKey) || userbank == null) {
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(OrderService.createInstance().getClientOrdersData(clientKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/orders/{orderKey}")
	@Produces({ "application/json" })
	public Response getOrderDetail(@PathParam("userbank") Integer number,
			@PathParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		
		return Response.ok().entity(OrderService.createInstance().getClientOrderDetail(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/orders/{orderKey}/amortisations")
	@Produces({ "application/json" })
	public Response getOrderAmortisations(@PathParam("userbank") Integer userbank,
			@PathParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (StringUtils.isEmpty(orderKey) || userbank == null) {
			Ivy.log().error("order key or bank number is missing");
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(AmortisationService.createInstance().get(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/orders/{orderKey}/collateral")
	@Produces({ "application/json" })
	public Response getOrderCollateral(@PathParam("userbank") Integer userbank,
			@PathParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (StringUtils.isEmpty(orderKey) || userbank == null) {
			Ivy.log().error("order key or bank number is missing");
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(CollateralService.createInstance().getClientOrderCollateralData(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/orders/{orderKey}/mortgagecertificates")
	@Produces({ "application/json" })
	public Response getMortgageCertificates(@PathParam("userbank") Integer userbank,  @PathParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (StringUtils.isEmpty(orderKey) || userbank == null) {
			Ivy.log().error("order key or bank number is missing");
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(MortgageCertificatesService.createInstance().getMortgageCertificatesByOrderKey(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/orders/{orderKey}/securities")
	@Produces({ "application/json" })
	public Response getSecurities(@PathParam("userbank") Integer number,
			@PathParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(number) || StringUtils.isBlank(orderKey)) {
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(SecurityService.createInstance().getSecuritiesByOrderKey(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/Currencies")
	@Produces({ "application/json" })
	public Response getCurrencies(@PathParam("userbank") Integer userbank,
			@Context ContainerRequestContext context) {
		Ivy.log().info("#####getCurrencies with Authorization access token: {0}", context.getHeaderString("Authorization"));
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(CurrenciesService.createInstance().getCurrencies()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/PeriodUnits")
	@Produces({ "application/json" })
	public Response getPeriodUnits(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(PeriodUnitsService.createInstance().getPeriodUnits()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/CollateralSubtypes")
	@Produces({ "application/json" })
	public Response getCollateralSubtypes(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(CollateralSubtypeService.createInstance().getCollateralSubtypes()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/LoanRequestReasons")
	@Produces({ "application/json" })
	public Response getLoanRequestReasons(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(LoanRequestReasonsService.createInstance().getLoanRequestReasons()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/RepaymentTypes")
	@Produces({ "application/json" })
	public Response getLoanRepaymentTypes(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(LoanRepaymentTypesService.createInstance().getLoanRepaymentTypes()).build();
	}
}
