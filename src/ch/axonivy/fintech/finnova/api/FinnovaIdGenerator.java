package ch.axonivy.fintech.finnova.api;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;

public final class FinnovaIdGenerator {
	private static Random rand = new Random();
	private static DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
	
	private FinnovaIdGenerator() { }
	
	public static String randomNumber() {
		long l = Math.abs(rand.nextLong());
		df.setMaximumFractionDigits(340);
		return df.format(l).substring(0, 15);
	}
	
	public static String formatExternalNumber(String number) {
		return String.join(".", number.split("(?<=\\G...)"));
	}
}
