package ch.axonivy.fintech.finnova.api;

import java.io.IOException;
import java.util.Objects;

import javax.annotation.security.PermitAll;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.CoreException;

import ch.axonivy.fintech.finnova.service.businessserviceadapter.AddressDataService;
import ch.axonivy.fintech.finnova.service.businessserviceadapter.ClientDataService;
import ch.axonivy.fintech.finnova.service.businessserviceadapter.CodeTablesService;
import ch.axonivy.fintech.finnova.service.businessserviceadapter.LoanOrderAdditionalFieldsDataService;
import ch.axonivy.fintech.finnova.service.businessserviceadapter.LoanOrderDataService;
import ch.axonivy.fintech.finnova.service.clientoverview.ClientMainTypeService;
import ch.axonivy.fintech.finnova.service.clientoverview.ClientStatusService;
import ch.axonivy.fintech.finnova.service.clientoverview.FinancesService;
import ch.axonivy.fintech.finnova.service.clientoverview.PrivateIndividualService;
import ch.axonivy.fintech.finnova.service.clientoverview.TwoPersonHouseholdService;
import ch.axonivy.fintech.finnova.service.loanadvisory.PricingService;
import ch.axonivy.fintech.finnova.service.loanadvisory.ProductTariffsService;
import ch.axonivy.fintech.finnova.service.realestate.CountriesService;
import ch.axonivy.fintech.finnova.service.realestate.PropertyService;
import ch.axonivy.fintech.finnova.service.realestate.PropertyTypesOfUseService;
import ch.axonivy.fintech.finnova.service.realestate.PropertyTypesService;
import ch.ivyteam.ivy.environment.Ivy;

@Singleton
@Path("v1")
public class V1Api {

	@GET
	@Path("/banks/{userbank}/clientData/")
	@Produces({ "application/json" })
	public Response getClientData(@PathParam("userbank") Integer userbank,
			@QueryParam("clientNumber") Long clientNumber,
			@QueryParam("allClients") Boolean allClients,
			@Context SecurityContext securityContext) throws NotFoundException, IOException, CoreException {
		if (Objects.isNull(userbank) || Objects.isNull(clientNumber) || Objects.isNull(allClients)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(ClientDataService.createInstance().getClientData(clientNumber.toString(), allClients)).build();
		
	}

	@GET
	@Path("/banks/{userbank}/addressData/")
	@Produces({ "application/json" })
	public Response getAddressData(@PathParam("userbank") Integer userbank,
			@QueryParam("clientNumber") Long clientNumber,
			@QueryParam("allAddresses") Boolean allAddresses,
			@Context SecurityContext securityContext) throws NotFoundException {
		if (Objects.isNull(userbank) || Objects.isNull(clientNumber) || Objects.isNull(allAddresses)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(AddressDataService.createInstance().getAddressData(clientNumber.toString(), allAddresses)).build();
	}

	@GET
	@Path("/banks/{userbank}/codeTables")
	@Produces({ "application/json" })
	public Response getCodeTables(@PathParam("userbank") Integer userbank,
			@Context ContainerRequestContext context) throws NotFoundException {
		Ivy.log().info("#####getCodeTables with Authorization : {0}", context.getHeaderString("Authorization"));
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(CodeTablesService.createInstance().getCodeTables()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/properties/{propertyKey}")
	@Produces({ "application/json" })
	@PermitAll
	public Response getProperty(@PathParam("userbank") Integer userbank,
			@PathParam("propertyKey") String propertyKey, @QueryParam("projection") String projection){
		if (StringUtils.isEmpty(propertyKey) || userbank == null || StringUtils.isEmpty(projection)) {
			Ivy.log().error("property key or bank number is missing");
			throw new InternalServerErrorException("Please take a look at server log for more detail!");
		}
		return Response.ok().entity(PropertyService.createInstance().getProperty(propertyKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/PropertyTypes")
	@Produces({ "application/json" })
	@PermitAll
	public Response getPropertyTypes(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(PropertyTypesService.createInstance().getPropertyTypes()).build();
	}
	

	@GET
	@Path("/banks/{userbank}/codes/PropertyTypesOfUse")
	@Produces({ "application/json" })
	@PermitAll
	public Response getPropertyTypesOfUse(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(PropertyTypesOfUseService.createInstance().getPropertyTypesOfUse()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/Countries")
	@Produces({ "application/json" })
	@PermitAll
	public Response getCountries(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(CountriesService.createInstance().getCountries()).build();
	}

	@GET
	@Path("/banks/{userbank}/loanOrderData")
	@Produces({ "application/json" })
	public Response getLoanOrderData(@PathParam("userbank") Integer userbank,
			@QueryParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)
			|| StringUtils.isAnyEmpty(orderKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(LoanOrderDataService.createInstance().getLoanOrderData(orderKey))
				.build();
	}
	
	@GET
	@Path("/banks/{userbank}/loanOrderAdditionalFieldsData")
	@Produces({ "application/json" })
	public Response getLoanOrderAdditionalFieldsData(@PathParam("userbank") Integer userbank,
			@QueryParam("orderKey") String orderKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)
			|| StringUtils.isAnyEmpty(orderKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(LoanOrderAdditionalFieldsDataService.createInstance().getLoanOrderAdditionalFieldsData(orderKey))
				.build();
	}

	@GET
	@PermitAll
	@Path("/banks/{userbank}/clients/{clientKey}/finances")
	@Produces({ "application/json" })
	public Response getFinances(@PathParam("userbank") Integer userbank, @PathParam("clientKey") String clientKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank) || StringUtils.isAnyEmpty(clientKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(FinancesService.createInstance().getFinances(clientKey)).build();
	}
	
	@GET
	@PermitAll
	@Path("/banks/{userbank}/clients/{clientKey}/clientMainType")
	@Produces({ "application/json" })
	public Response getClientMainType(@PathParam("userbank") Integer userbank, @PathParam("clientKey") String clientKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank) || StringUtils.isAnyEmpty(clientKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(ClientMainTypeService.createInstance().getFinnovaData(clientKey)).build();
	}
	
	@GET
	@PermitAll
	@Path("/banks/{userbank}/clients/{clientKey}/privateIndividual")
	@Produces({ "application/json" })
	public Response getPrivateIndividual(@PathParam("userbank") Integer userbank, @PathParam("clientKey") String clientKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank) || StringUtils.isAnyEmpty(clientKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(PrivateIndividualService.createInstance().getFinovaData(clientKey)).build();
	}

	@GET
	@PermitAll
	@Path("/banks/{userbank}/clients/{clientKey}/twoPersonHousehold")
	@Produces({ "application/json" })
	public Response getTwoPeronHousehold(@PathParam("userbank") Integer userbank, @PathParam("clientKey") String clientKey,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank) || StringUtils.isAnyEmpty(clientKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(TwoPersonHouseholdService.createInstance().getFinovaData(clientKey)).build();
	}
	
	@GET
	@PermitAll
	@Path("/banks/{userbank}/loanAdvisories/pricing")
	@Produces({ "application/json" })
	public Response getPricing(@PathParam("userbank") Integer userbank,
			@QueryParam("orderKey") String orderKey,
			@QueryParam("ownerOccupied") Boolean ownerOccupied,
			@QueryParam("propertyKey") String propertyKey,
			@QueryParam("businessCase") Long businessCase,
			@QueryParam("formOfUse") Long formOfUse ,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank) || Objects.isNull(ownerOccupied) || Objects.isNull(businessCase)
				|| Objects.isNull(formOfUse) || StringUtils.isAnyEmpty(orderKey, propertyKey)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(PricingService.createInstance().getPricings(orderKey)).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/ProductTariffs")
	@Produces({ "application/json" })
	@PermitAll
	public Response getProductTariffs(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(ProductTariffsService.createInstance().getProductTariffs()).build();
	}
	
	@GET
	@Path("/banks/{userbank}/codes/ClientStatus")
	@Produces({ "application/json" })
	@PermitAll
	public Response getClientStatuses(@PathParam("userbank") Integer userbank,
			@Context SecurityContext securityContext) {
		if (Objects.isNull(userbank)) {
			return Response.status(Status.METHOD_NOT_ALLOWED).entity("Required params").build();
		}
		return Response.ok().entity(ClientStatusService.createInstance().getClientStatuses()).build();
	}
}
