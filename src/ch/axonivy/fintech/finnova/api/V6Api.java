package ch.axonivy.fintech.finnova.api;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.security.PermitAll;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

@PermitAll
@Path("v6/banks/{bankId}")
@Produces("application/json")
public class V6Api {
	private static final int NUMBER_OF_DUPLICATES = 30;
	
	@Context
    HttpHeaders httpHeaders;
	
	@GET
	@Path("clients")
	public Response duplicateCheck(@QueryParam("firstName") String firstName,
			@QueryParam("lastName") String lastName,
			@QueryParam("firstNamePartner") String firstNamePartner,
			@QueryParam("lastNamePartner") String lastNamePartner,
			@QueryParam("dateOfBirth") String dateOfBirth,
			@QueryParam("dateOfBirthPartner") String dateOfBirthPartner) {
		validateToken();
		
		if (firstName == null || firstName.trim().isEmpty()) {
			return Response.status(400).entity("{\"error\":\"firstname required\"}").build();
		}

		if ((firstName.contains("noduplicate")) ||
				(lastName != null && lastName.contains("noduplicate"))) {
			return Response.ok(java.util.Collections.EMPTY_LIST).build();
		}
		
		
		List<PossibleDuplicate> duplicates = IntStream.range(0, NUMBER_OF_DUPLICATES).mapToObj(i -> {
			PossibleDuplicate possibleDuplicate = new PossibleDuplicate();
			possibleDuplicate.id = FinnovaIdGenerator.randomNumber();
			possibleDuplicate.number = FinnovaIdGenerator.randomNumber();
			possibleDuplicate.dateOfBirth = "11.11.198" + (i % 10 );
			possibleDuplicate.externalNumber = FinnovaIdGenerator.formatExternalNumber(possibleDuplicate.number);
			possibleDuplicate.firstName = firstName + i;
			possibleDuplicate.lastName = lastName + i;
			possibleDuplicate.canton = "Canton " + i;
			possibleDuplicate.place = "Place " + i;
			possibleDuplicate.postCode = 1111 + i;
			possibleDuplicate.clientIdentified = false;
			return possibleDuplicate;
		}).collect(Collectors.toList());
		return Response.ok(duplicates).build();
	}
	
	@POST
	@Path("clients/{clientId}/accounts")
	public Response reserveAccountNumber(@PathParam("clientId") String clientId, @PathParam("bankId") String bankId) {
		validateToken();
		
		IdReservationResult account = new IdReservationResult();
		account.id = FinnovaIdGenerator.randomNumber();
		account.number = FinnovaIdGenerator.randomNumber();
		account.externalNumber = FinnovaIdGenerator.formatExternalNumber(account.number);
		
		return Response.created(URI.create("banks/"+ bankId + "/clients/" + clientId + "/accounts/" + account.id))
				.entity(account).build();
	}
	
	@POST
	@Path("clients")
	public Response reserveClient(@PathParam("bankId") String bankId) {
		validateToken();
		
		IdReservationResult client = new IdReservationResult();
		client.id = FinnovaIdGenerator.randomNumber();
		client.number = FinnovaIdGenerator.randomNumber();
		client.externalNumber = FinnovaIdGenerator.formatExternalNumber(client.number);
		
		return Response.created(URI.create("banks/"+ bankId + "/clients" + client.id))
				.entity(client).build();
	}
	
	
	/**
	 * Handle every unknown PUT request by returning a 200 success response if authenticated with some token
	 */
	@PUT
	@Path("/{path: .*}")
	@PermitAll
	public Response anyOtherPut(@HeaderParam("Authorization") String authHeader, Object body) {
		validateToken();
		return Response.ok(body).build();
	}
	
	
	
	void validateToken() {
		String authHeader = httpHeaders.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (authHeader == null || !authHeader.matches("Bearer\\s+[\\d\\w-]+")) {
			throw new ForbiddenException();
		}
	}
	
	public static class IdReservationResult {
		public String id;
		public String number;
		public String externalNumber;
	}
	

	public class PossibleDuplicate {
		public String number;
		public String externalNumber;
		public String id;
		public String firstName;
		public String lastName;
		public String clientDesignation;
		public String clientHeaderRow2;
		public String clientHeaderRow3;
		public Integer postCode;
		public String place;
		public String canton;
		public String dateOfBirth;
		public Boolean clientIdentified;
		public Integer clientStatus;
		public String closureDate;
		public Map<String, Object> dynamicValues;
	}
}
