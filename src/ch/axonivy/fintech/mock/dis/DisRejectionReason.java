package ch.axonivy.fintech.mock.dis;

public enum DisRejectionReason {
	
	REJECTION_NO_DATA_MATCH("No data match", "RejectionNoDataMatch", 
			"The identification data recorded by the end user are not consistent with the information contained in the identity document"), 
	REJECTION_NEGATIVE_PROFILING("Wrong photograph - negative profiling", 
			"RejectionNegativeProfiling", "Photograph contained in the identity document is not consistent with the person to be identified (negative profiling)."), 
	REJECTION_FALSE_BIRTHDATE("Wrong birthday", "RejectionFalseBirthdate", "Date of birth differs from the date of birth recorded."), 
	REJECTION_MANIPULATIVE_TRY("Manipulative try", "RejectionManipulativeTry", 
			"Attempt at manipulation by the person to be identified / unusual behaviour by the individual "
			+ "/ strong indications of attempted fraud."), 
	REJECTION_FALSE_PERS_ATTRIBUTES("Person hesitates on personal data", "RejectionFalsePersAttributes", 
			"Person hesitates / is uncertain in relation to fundamentally known attributes such as: "
			+ "firstname(s), date of birth or place of birth/place of origin."),
	REJECTION_FRAUD_DOCUMENT("Identity document fraud", "RejectionFraudDocument", 
			"The identity document shows indications of fraud, identity document contains invalid holograms or has expired."), 
	REJECTION_MRZ_CHECK_DIGIT_ERROR("Machine Readable Zone calculation error", "RejectionMRZCheckDigitError", 
			"Check digit calculation in the MRZ (Machine Readable Zone) gives an error."), 
	REJECTION_DIFFERING_AGE("Wrong expiry date vs age of the person", "RejectionDifferingAge", 
			"Expiry date of the identity document is not consistent with the age of the person to be identified."), 
	REJECTION_AGAIN("Already rejected today", "RejectionAgain", "Identification has already been refused on the same day"), 
	REJECTION_NO_DATA_MATCH_AND_MANIPULATIVE_TRY("Inconsistent data and manipulation try", "RejectionNoDataMatchAndManipulativeTry", 
			"The identification data recorded by the end user are not consistent "
			+ "with the information contained in the identity document. Attempt at "
			+ "manipulation by the person to be identified / unusual behaviour by the "
			+ "individual / strong indications of attempted fraud."),
	REJECTION_NO_DATA_MATCH_AND_FALSE_PERS_ATTRIBUTES("Inconsistent data and person hesitates", "RejectionNoDataMatchAndFalsePersAttributes", 
			"The identification data recorded by the end user are not consistent "
			+ "with the information contained in the identity document. Person hesitates / "
			+ "is uncertain in relation to fundamentally known attributes such as: first name(s), date of birth or place of birth/place of origin."), 
	REJECTION_NO_DATA_MATCH_AND_NEGATIVE_PROFILING("Inconsistent identification data and negative profiling", "RejectionNoDataMatchAndNegativeProfiling", 
			"The identification data recorded by the end user are not consistent "
			+ "with the information contained in the identity document. Photograph contained in the identity document is not "
			+ "consistent with the person to be identified (negative profiling)."), 
	REJECTION_NO_DATA_MATCH_AND_FALSE_BIRTHDATE("Inconsistent identification data and wrong birthday", "RejectionNoDataMatchAndFalseBirthdate", 
			"The identification data recorded by the end user are not consistent "
			+ "with the information contained in the identity document. Date of birth differs from the date of birth recorded.")
	;

	private String displayName;
	private String description;
	private String value;
	
	private DisRejectionReason(String displayName, String value, String description) {
		this.description = description;
		this.displayName = displayName;
		this.value = value;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getValue() {
		return value;
	}
}
