package ch.axonivy.fintech.mock.dis.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import org.bouncycastle.util.io.Streams;
import ch.ivyteam.ivy.cm.IContentObject;
import ch.ivyteam.ivy.cm.IContentObjectValue;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.File;

public class PGPUtils {
	
	private static final ch.ivyteam.log.Logger LOG = Ivy.log();

	/**
	 * decrypt the passed in message stream
	 */
	 public static void decryptFile(String inputFileName, String keyFileName, char[] passwd, String defaultFileName)
			 throws IOException, NoSuchProviderException {
		File nfi = new File("private_key.txt");
		if (!nfi.exists()) {
			LOG.info("[DISMOCK INFO] The private_key.txt Ivy File does not exist. We create the private key with the cms content {0}", "/PGPEncryption/private_key");
			java.io.File nf = nfi.getJavaFile();
			IContentObject co = Ivy.cms().findContentObject("/PGPEncryption/private_key");
			IContentObjectValue cov = co.getValues().get(0);
			cov.exportContentToFile(nf, null);
			LOG.info("[DISMOCK INFO] private_key.txt Ivy File created at {0}", nf.getAbsolutePath());
		}
		InputStream in = new BufferedInputStream(new FileInputStream(inputFileName));
		InputStream keyIn = new BufferedInputStream(new FileInputStream(keyFileName));
		decryptFile(in, keyIn, passwd, defaultFileName);
		keyIn.close();
		in.close();
	 }
	 
	 public static void encryptFile( String outputFilePath, String inputFilePath, String encKeyFileName, boolean armor, boolean withIntegrityCheck)
			 throws IOException, NoSuchProviderException, PGPException {
		 File nfi = new File(encKeyFileName);
		 if (!nfi.exists()) {
			 LOG.info("[DISMOCK INFO] The {0} Ivy File does not exist. We create the public key with the cms content {1}", encKeyFileName, "/PGPEncryption/public_key");
			 java.io.File nf = nfi.getJavaFile();
			 IContentObject co = Ivy.cms().findContentObject("/PGPEncryption/public_key");
			 IContentObjectValue cov = co.getValues().get(0);
			 cov.exportContentToFile(nf, null);
			 LOG.info("[DISMOCK INFO] public key Ivy File created at {0}", nf.getAbsolutePath());
		 }
		 java.io.File outputFile = new java.io.File(outputFilePath);
		 if(!outputFile.exists()) {
			 outputFile.createNewFile();
			 LOG.info("[DISMOCK INFO] output file {0} created", outputFile);
		 }
		 LOG.info("[DISMOCK INFO] Encrypting the file {0} into {1} with public_key located at {2}", inputFilePath, outputFilePath, nfi.getAbsolutePath());
		 
		 OutputStream out = new BufferedOutputStream(new FileOutputStream(outputFilePath));
		 
		 PGPPublicKey encKey = PGPSecretUtils.readPublicKey(nfi.getAbsolutePath());
		 encryptFile(out, inputFilePath, encKey, armor, withIntegrityCheck);
		 out.close();
	 }

	 public static boolean isEmptyFile(String inputFileName) throws IOException {
		 FileInputStream fis = null;
		 try {
			 fis = new FileInputStream(inputFileName);
			 int byteValue = fis.read();
			 if (byteValue == -1) { // file content is empty
				 return true;
			 }
		 } catch (Exception e) {
			 LOG.error("[DISMOCK ERROR] Exception occurred while reading file {0} for ", e, inputFileName);
		 } finally {
			 fis.close();
		 }

		 return false;
	 }
	 
	 public static Boolean copyFile(String inputFileName, String outputFileName) {
		 Boolean success = false;
		 try {
			 java.io.File ipf = new java.io.File(inputFileName); 
			 java.io.File opf = new java.io.File(outputFileName); 
			 if (!ipf.exists()) {
				 return false;
			 }
			 FileUtils.copyFile(ipf, opf);
			 success = true;
		 }
		 catch (Exception e) {
			 LOG.error("[DISMOCK ERROR] Exception occurred while copying {0} to {1}", e, inputFileName, outputFileName);
			 success = false;
		 }
		 return success;
	 }

	 /**
	  * decrypt the passed in message stream
	  */
	 private static void decryptFile(InputStream in, InputStream keyIn, char[] passwd, String defaultFileName)
			 throws IOException, NoSuchProviderException {    
		 Security.addProvider(new BouncyCastleProvider());
		 in = PGPUtil.getDecoderStream(in);

		 try {
			 JcaPGPObjectFactory pgpF = new JcaPGPObjectFactory(in);
			 PGPEncryptedDataList enc;

			 Object o = pgpF.nextObject();
			 //
			 // the first object might be a PGP marker packet.
			 //
			 if (o instanceof PGPEncryptedDataList) {
				 enc = (PGPEncryptedDataList)o;
			 } else {
				 enc = (PGPEncryptedDataList)pgpF.nextObject();
			 }

			 //
			 // find the secret key
			 //
			 Iterator<?> it = enc.getEncryptedDataObjects();
			 PGPPrivateKey sKey = null;
			 PGPPublicKeyEncryptedData pbe = null;
			 PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(
					 PGPUtil.getDecoderStream(keyIn), new JcaKeyFingerprintCalculator());

			 while (sKey == null && it.hasNext()) {
				 pbe = (PGPPublicKeyEncryptedData) it.next();

				 sKey = PGPSecretUtils.findSecretKey(pgpSec, pbe.getKeyID(), passwd);
			 }

			 if (sKey == null) {
				 throw new IllegalArgumentException("secret key for message not found.");
			 }

			 InputStream clear = pbe.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider("BC").build(sKey));

			 JcaPGPObjectFactory plainFact = new JcaPGPObjectFactory(clear);
			 Object message = plainFact.nextObject();

			 if(message instanceof PGPCompressedData) {
				 PGPCompressedData   cData = (PGPCompressedData) message;
				 InputStream         compressedStream = new BufferedInputStream(cData.getDataStream());
				 JcaPGPObjectFactory    pgpFact = new JcaPGPObjectFactory(compressedStream);
				 message = pgpFact.nextObject();
			 }

			 if (message instanceof PGPLiteralData) {
				 PGPLiteralData ld = (PGPLiteralData)message;

				 String outFileName = ld.getFileName();
				 if (!defaultFileName.isEmpty()) {
					 outFileName = defaultFileName;
				 }

				 InputStream unc = ld.getInputStream();
				 OutputStream fOut =  new BufferedOutputStream(new FileOutputStream(outFileName));

				 Streams.pipeAll(unc, fOut);

				 fOut.close();
			 } else if (message instanceof PGPOnePassSignatureList) {
				 throw new PGPException("encrypted message contains a signed message - not literal data.");
			 } else {
				 throw new PGPException("message is not a simple encrypted file - type unknown.");
			 }

			 if (pbe.isIntegrityProtected()) {
				 if (!pbe.verify()) {
					 LOG.error("[DISMOCK ERROR] The PGPPublicKeyEncryptedData does not pass the integrity check");
				 }
				 else {
					 LOG.info("[DISMOCK INFO] The PGPPublicKeyEncryptedData passes the integrity check");
				 }
			 } else {
				 LOG.info("[DISMOCK INFO] The PGPPublicKeyEncryptedData is not integrity protected");
			 }
		 } catch (PGPException e) {
			 LOG.error("[DISMOCK ERROR] PGPException occurred while decrypting the file", e);
			 if (e.getUnderlyingException() != null) {
				 LOG.error("[DISMOCK ERROR] PGPException underlying exception", e.getUnderlyingException());
			 }
		 }
	 }

	 private static void encryptFile( OutputStream out, String fileName, PGPPublicKey encKey, boolean armor, boolean withIntegrityCheck)
			 throws IOException, NoSuchProviderException {
		 Security.addProvider(new BouncyCastleProvider());
		 if (armor) {
			 out = new ArmoredOutputStream(out);
		 }

		 try {    
			 PGPEncryptedDataGenerator   cPk = new PGPEncryptedDataGenerator(
					 new JcePGPDataEncryptorBuilder(PGPEncryptedData.CAST5)
					 .setWithIntegrityPacket(withIntegrityCheck).setSecureRandom(new SecureRandom()).setProvider("BC")
					 );
			 cPk.addMethod(new JcePublicKeyKeyEncryptionMethodGenerator(encKey).setProvider("BC"));
			 OutputStream cOut = cPk.open(out, new byte[1 << 16]);
			 PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
			 PGPUtil.writeFileToLiteralData(comData.open(cOut), PGPLiteralData.BINARY, new java.io.File(fileName), new byte[1 << 16]);

			 comData.close();
			 cOut.close();
			 if (armor) {
				 out.close();
			 }
		 } catch (PGPException e) {
			 LOG.error("[DISMOCK ERROR] PGPException occurred while encrypting the file", e);
			 if (e.getUnderlyingException() != null) {
				 LOG.error("[DISMOCK ERROR] PGPException underlying exception", e.getUnderlyingException());
			 }
		 }
	 }

}
