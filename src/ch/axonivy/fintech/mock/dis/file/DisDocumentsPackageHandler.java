package ch.axonivy.fintech.mock.dis.file;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.DateTime;
import ch.ivyteam.ivy.scripting.objects.File;

public class DisDocumentsPackageHandler {
	
	private static final String BASE_AUDIO_FILE_NAME = "DIS_audio_5_TRANSACTIONID_182050001_20180425090911115050.mp3";
	private static final String BASE_DOC_FILE_NAME = "DIS_doc_1_TRANSACTIONID_182050001_20180425090911115050.pdf";
	private static final String BASE_PASS_2_FILE_NAME = "DIS_pass_2_TRANSACTIONID_182050001_20180425090911115050.jpg";
	private static final String BASE_PASS_3_FILE_NAME = "DIS_pass_3_TRANSACTIONID_182050001_20180425090911115050.jpg";
	private static final String BASE_PORTRAIT_FILE_NAME = "DIS_portrait_4_TRANSACTIONID_182050001_20180425090911115050.jpg";
	private static final String BASE_SWISSCOM_IDENT_FILE_NAME = "Swisscom_ident_TRANSACTIONID_182050001_20180425090911115050.pdf";
	
	public DisDocumentsPackageHandler() {
		
	}
	
	public boolean sendDisZipPackageForDossier(String cobId) {
		DateTime dt = new DateTime();
		String zipFileName = "DISMOCK_00125__177672700_" + cobId + "_" + dt.toNumber() + ".zip";
		String zipFilePath = Ivy.wf().getApplication().getFileArea().getParentFile().getAbsolutePath() + "/" + zipFileName;
		
		try {
			File tempZipFile = makeTempZip(cobId, zipFileName);
			Ivy.log().debug("[DISMOCK DEBUG {0}] Temp DIS Zip File made : {1}", cobId, tempZipFile);
			
			boolean encryptSuccess = false;
			try {
				String tempPGPFilePath = tempZipFile.getAbsolutePath() + ".pgp";
				PGPUtils.encryptFile(tempPGPFilePath, tempZipFile.getAbsolutePath(), "public_key.txt", false, false);
				FileUtils.copyFile(new java.io.File(tempPGPFilePath), new java.io.File(zipFilePath + ".pgp"));
				Ivy.log().debug("[DISMOCK DEBUG {0}] Temp DIS PGP File copied to : {1}", cobId, zipFilePath + ".pgp");
				encryptSuccess = true;
			} catch (Exception e) {
				Ivy.log().error("[DISMOCK ERROR " + cobId + "] Exception while encrypting file " + tempZipFile.getAbsolutePath(), e);
			}
			if(!encryptSuccess) {
				FileUtils.copyFile(tempZipFile.getJavaFile(), new java.io.File(zipFilePath));
				Ivy.log().debug("[DISMOCK DEBUG {0}] Temp DIS Zip File moved to {1}", cobId, zipFilePath);
			}
			
			return true;
		} catch (Exception e) {
			Ivy.log().error("[DISMOCK ERROR " + cobId + "] Exception while sending DIS documents Zip package to " + zipFileName, e);
		}
		return false;
	}

	private File makeTempZip(String cobId, String zipFileName) throws IOException, URISyntaxException {
		
		List<File> filesInDISPackage = new ArrayList<>();
		
		filesInDISPackage.add(makeDisFile(BASE_AUDIO_FILE_NAME, cobId));
		filesInDISPackage.add(makeDisFile(BASE_SWISSCOM_IDENT_FILE_NAME, cobId));
		
		File docFile = makeDocFile(cobId);
		filesInDISPackage.add(docFile);
		
		filesInDISPackage.add(makeDisFile(BASE_PASS_2_FILE_NAME, cobId));
		filesInDISPackage.add(makeDisFile(BASE_PASS_3_FILE_NAME, cobId));
		filesInDISPackage.add(makeDisFile(BASE_PORTRAIT_FILE_NAME, cobId));
		
		File ivyZipFile = new File(zipFileName, true);
		
		
		Map<String, String> zip_properties = new HashMap<>(); 
		zip_properties.put("create", "true");
        zip_properties.put("encoding", "UTF-8");
        
        try (FileSystem zipfs = FileSystems.newFileSystem(URI.create("jar:" + ivyZipFile.getJavaFile().toPath().toUri().toString()), zip_properties, null)) {
        	filesInDISPackage.forEach(f -> {
                Path pathInZipfile = zipfs.getPath("/" + f.getName());
                try {
					Files.copy(f.getJavaFile().toPath(), pathInZipfile, 
					        StandardCopyOption.REPLACE_EXISTING );
				} catch (Exception e) {
					Ivy.log().error("[DISMOCK ERROR " + cobId + "] Exception while adding "+ f.getName() + " to Zip package" , e);
				} 
        	});
        }
        
        try {
        	Ivy.log().debug("[DISMOCK DEBUG {0}] The  doc file has been deleted ? {1}", cobId, docFile.delete());
        } catch (Exception e) {
        	Ivy.log().error("[DISMOCK ERROR {0}] An error occurred while deleting the doc file {1} {2}", cobId, docFile, e.getMessage(), e);
        }
        
		return ivyZipFile;
	}

	private File makeDocFile(String cobId) throws IOException,
			URISyntaxException {
		File docIvyFile;
		java.io.File docFile = getDocFileIfAlreadyExist();
		if(docFile != null) {
			docIvyFile = new File("MOCKDIS/" + docFile.getName());
			docIvyFile = docIvyFile.rename(BASE_DOC_FILE_NAME.replaceFirst("TRANSACTIONID", cobId), true);
		} else {
			docIvyFile = makeDisFile(BASE_DOC_FILE_NAME, cobId);
		}
		return docIvyFile;
	}

	private java.io.File getDocFileIfAlreadyExist() {
		java.io.File docDirectory = new java.io.File(Ivy.wf().getApplication().getFileArea().getAbsolutePath() + "/MOCKDIS");
		java.io.File docFile = null;
		if(docDirectory.isDirectory()) {
			for(java.io.File f: docDirectory.listFiles()) {
				if(f.getName().endsWith(".pdf")) {
					docFile = f;
					break;
				}
			}
		}
		return docFile;
	}
	
	private File makeDisFile(String fileBaseName, String cobId) throws IOException, URISyntaxException {
		String audioFileName = fileBaseName.replaceFirst("TRANSACTIONID", cobId);
		File ivyFile = new File(audioFileName, true);
		FileUtils.copyFile(new java.io.File(DisDocumentsPackageHandler.class.getResource(fileBaseName).toURI().getPath()), ivyFile.getJavaFile());
		return ivyFile;
	}
	
	

}
