package ch.axonivy.fintech.mock.dis;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="disRejectionReasonBean")
public class DisRejectionReasonBean {
	
	public DisRejectionReason[] getReasons() {
		return DisRejectionReason.values();
	}

}
