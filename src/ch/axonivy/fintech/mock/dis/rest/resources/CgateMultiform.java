package ch.axonivy.fintech.mock.dis.rest.resources;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;

import ch.axonivy.fintech.mock.dis.data.Msg;
import ch.axonivy.fintech.mock.dis.data.ResultMessage;
import ch.ivyteam.api.API;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.process.call.ISubProcessStart;
import ch.ivyteam.ivy.process.call.SubProcessRunner;
import ch.ivyteam.ivy.process.call.SubProcessSearchFilter;

@Path("cgate/multiform")
public class CgateMultiform {

	private static final String UTF_8 = "UTF-8";
	private static final String ISO_8859_1 = "ISO-8859-1";

	@POST
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMessage postSignPerson(
			@Context UriInfo uriInfo, String signPersonStringParams) throws UnsupportedEncodingException, URISyntaxException {
		
		ResultMessage resultMessage = new ResultMessage();
		resultMessage.setResult(true);
		Msg m = new Msg();
		
		Ivy.log().debug("[DISMOCK DEBUG cgate/multiform] Params get from caller: {0}", signPersonStringParams);
		
		String videoRedirectUrl = getUrl();
		if(!StringUtils.isBlank(signPersonStringParams)) {
			signPersonStringParams = URLDecoder.decode(signPersonStringParams, UTF_8);
			fillMessageFieldsWithStringParameters(signPersonStringParams, m);
			videoRedirectUrl += "?" + encodeUrlParams(signPersonStringParams);
		} 
		
		m.setUrl(normalizePortInUrl(videoRedirectUrl));
		fillMessageFieldsWithQueryParameters(uriInfo.getQueryParameters(), m);
		
		Ivy.log().debug("[DISMOCK DEBUG cgate/multiform] url where the video is redirected: {0}", m.getUrl());
		
		resultMessage.setResponse("");
		resultMessage.setMsg(m);
		return resultMessage;
	}

	private String encodeUrlParams(String signPersonStringParams) throws UnsupportedEncodingException {
		String result = "";
		for(String paramPairs: signPersonStringParams.split("&")) {
			if(!result.isEmpty()) {
				result += "&";
			}
			String[] paramKeyValue = paramPairs.split("=");
			if(paramKeyValue.length > 1) {
				result += paramKeyValue[0] + "=" + URLEncoder.encode(paramKeyValue[1], ISO_8859_1);
			}
		}
		return result;
	}
	


	private String getUrl() {
		try{
			ISubProcessStart subProcessStart = getSubProcess("getProcessUrl");
			if(subProcessStart == null) {
				Ivy.log().error("[DISMOCK ERROR cgate/multiform] getProcessUrl could not be found");
			} else {
				Object [] results = SubProcessRunner.exec(subProcessStart, "Functional Processes/dis/videoIdentification/videoIdentification.ivp");
				if(results.length > 0) {
					return (String) results[0];
				}
			}
		} catch(Exception e) {
			Ivy.log().error("[DISMOCK ERROR cgate/multiform] " + e.getMessage(), e);
		}
		return makeUrl();
	}



	private String makeUrl() {
		String url = null;
		String applicationHomeRef = Ivy.html().applicationHomeRef();
		Ivy.log().debug("[DISMOCK DEBUG cgate/multiform] " + Ivy.html().applicationHomeRef());
		try {
			
			url = getProtocolPartFromUrl(applicationHomeRef) + 
					InetAddress.getLocalHost().getHostAddress() + 
					getPortPartFromUrl(applicationHomeRef) + 
					"/ivy/pro/" +
					getApplicationName(applicationHomeRef) + 
					"/fintech_external_services_mock/162D9B06E7844FCB/videoIdentification.ivp";
		} catch (UnknownHostException e) {
			
		}
		return url;
	}
	
	private String getApplicationName(String applicationHomeRef) {
		int index = applicationHomeRef.indexOf("ivy/wf/") + 7;
		String appName = applicationHomeRef.substring(index);
		return appName.substring(0, appName.indexOf('/'));
	}

	private String getPortPartFromUrl(String url) {
		String port = url.substring(getProtocolPartFromUrl(url).length());
		port = port.substring(port.indexOf(':'), port.indexOf('/'));
		return port;
	}

	private String getProtocolPartFromUrl(String url) {
		return url.substring(0, url.indexOf(':')) + "://";
	}

	private void fillMessageFieldsWithQueryParameters(MultivaluedMap<String, String> multivaluedMap, Msg m) {
		multivaluedMap.forEach((k, v) -> {
			try {
				Field field = Msg.class.getDeclaredField(k);
				Ivy.log().debug("[DISMOCK DEBUG cgate/multiform] Found field {0}", k);
				field.setAccessible(true);
				field.set(m, v);
			} catch (Exception e) {
				// ignore
				Ivy.log().error("[DISMOCK ERROR cgate/multiform] Error " + e);
			}
			
		});
	}

	private void fillMessageFieldsWithStringParameters(
			String signPersonStringParams, Msg m) {
		String[] params = signPersonStringParams.split("&");
		for(String kv: params) {
			String[] keyValue = kv.split("=");
			if(keyValue.length == 2) {
				try {
					Field field = Msg.class.getDeclaredField(keyValue[0]);
					field.setAccessible(true);
					ArrayList<String> al = new ArrayList<>();
					al.add(keyValue[1]);
					field.set(m, al);
				} catch (Exception e) {
					// ignore
					Ivy.log().error("[DISMOCK ERROR cgate/multiform] Error " + e);
				}
			}
		}
	}
	
	private ISubProcessStart getSubProcess(String className){

	    SubProcessSearchFilter filter = SubProcessSearchFilter.create()
	            .setProcessPathPattern(className) 
	            .toFilter();


	    List<ISubProcessStart> foundSubProcesses = SubProcessRunner.findSubProcessStarts(filter);
	    if (foundSubProcesses.isEmpty()) {
	        return null;
	    }
	    return foundSubProcesses.get(0);
	}

	static String normalizePortInUrl(String url) throws URISyntaxException {
		API.checkNotEmpty(url, "url");
		
		URIBuilder builder = new URIBuilder(url);
		
		if(builder.getPort() == 0) {
			removePort(builder);
		}
		
		return builder.build().toString();
	}
	
	private static URIBuilder removePort(URIBuilder uriBuilder) {
		return uriBuilder.setPort(-1);
	}
	
}
