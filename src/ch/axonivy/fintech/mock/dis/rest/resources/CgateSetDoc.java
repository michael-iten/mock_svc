package ch.axonivy.fintech.mock.dis.rest.resources;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import ch.axonivy.fintech.mock.dis.data.ResultMessageDocument;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.scripting.objects.File;
import ch.ivyteam.util.Base64;

@Path("cgate/setdoc")
public class CgateSetDoc {
	
	private Map<String, String> parameters = new HashMap<String, String>();
	
	@POST
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMessageDocument setDoc(@Context UriInfo uriInfo, String setDocStringParams) {
		Ivy.log().debug("[DISMOCK DEBUG cgate/setdoc] Found param {0}", setDocStringParams);
		fillParameters(setDocStringParams);
		try {
			writeContractFile(setDocStringParams);
		} catch (IOException e) {
			Ivy.log().error("[DISMOCK ERROR cgate/setdoc] error occurred while writing the contract file ", e);
		}
		ResultMessageDocument result = new ResultMessageDocument();
		result.setResult(true);
		result.setMsg(setDocStringParams);
		
		return result;
	}

	private void fillParameters(String setDocStringParams) {
		String[] params = setDocStringParams.split("&");
		for(String kv: params) {
			String[] keyValue = kv.split("=");
			if(keyValue.length == 2) {
				try {
					parameters.put(keyValue[0], URLDecoder.decode(keyValue[1], "UTF-8"));
				} catch (Exception e) {
					Ivy.log().error("Error " + e);
				}
			}
		}
	}

	private void writeContractFile(String setDocStringParams) throws IOException {
		if(!setDocStringParams.contains("file_data=")) {
			return;
		}
		String filename = StringUtils.isBlank(parameters.get("file_name")) ? "testContractA" : parameters.get("file_name");
		
		if(StringUtils.isBlank(parameters.get("suffix"))) {
			filename += ".pdf";
		} else {
			filename +=  "." + parameters.get("suffix");
		}
		
		
		File contract = new File("MOCKDIS/" + filename);
		FileUtils.writeByteArrayToFile(contract.getJavaFile(), Base64.decode(parameters.get("file_data")));
		
		Ivy.log().debug("[DISMOCK DEBUG cgate/setdoc] The contract file has been written to {0}", contract.getAbsolutePath());
	}
	

}
