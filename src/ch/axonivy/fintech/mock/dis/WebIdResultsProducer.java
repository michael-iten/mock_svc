package ch.axonivy.fintech.mock.dis;

import com.google.gson.Gson;

import ch.axonivy.fintech.mock.dis.data.WebidResults;
import ch.ivyteam.util.Base64;

public class WebIdResultsProducer {
	
	public static String produceBase64Result(WebidResults webidResults) {
		return Base64.encode(new Gson().toJson(webidResults));
	}
	

}
