package ch.axonivy.fintech.mock.dis.data;

import java.io.Serializable;


public class ResultMessage implements Serializable {

	
	private static final long serialVersionUID = 3887838642175894915L;
	
	private Boolean result;
	private Msg msg;
	private String errType;
	private String response;
	
	public Boolean getResult() {
		return result;
	}
	
	public void setResult(Boolean result) {
		this.result = result;
	}
	
	public Msg getMsg() {
		return msg;
	}
	
	public void setMsg(Msg msg) {
		this.msg = msg;
	}
	
	public String getErrType() {
		return errType;
	}
	
	public void setErrType(String errType) {
		this.errType = errType;
	}
	
	public String getResponse() {
		return response;
	}
	
	public void setResponse(String response) {
		this.response = response;
	}

}
