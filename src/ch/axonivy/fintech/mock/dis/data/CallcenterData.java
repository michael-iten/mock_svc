
package ch.axonivy.fintech.mock.dis.data;

import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class CallcenterData {

    private Object callcenter_data_id;
    private Object callcenter_id;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public Object getCallcenter_data_id() {
		return callcenter_data_id;
	}

	public void setCallcenter_data_id(Object callcenter_data_id) {
		this.callcenter_data_id = callcenter_data_id;
	}

	public Object getCallcenter_id() {
		return callcenter_id;
	}

	public void setCallcenter_id(Object callcenter_id) {
		this.callcenter_id = callcenter_id;
	}

}
