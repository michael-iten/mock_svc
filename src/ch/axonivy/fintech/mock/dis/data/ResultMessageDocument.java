package ch.axonivy.fintech.mock.dis.data;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ResultMessageDocument {

	private Boolean result;
	private String msg;
	private String errType;
	private String response;

	/**
	 *
	 * @return
	 * The result
	 */
	public Boolean getResult() {
		return result;
	}

	/**
	 *
	 * @param result
	 * The result
	 */
	public void setResult(Boolean result) {
		this.result = result;
	}

	/**
	 *
	 * @return
	 * The msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 *
	 * @param msg
	 * The msg
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 *
	 * @return
	 * The errType
	 */
	public String getErrType() {
		return errType;
	}

	/**
	 *
	 * @param errType
	 * The errType
	 */
	public void setErrType(String errType) {
		this.errType = errType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
