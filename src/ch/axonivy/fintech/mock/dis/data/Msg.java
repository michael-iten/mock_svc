package ch.axonivy.fintech.mock.dis.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Msg implements Serializable {

	
	private static final long serialVersionUID = -1007863486527284779L;
	
	private List<String> gender = new ArrayList<String>();
	private List<String> firstname = new ArrayList<String>();
	private List<String> lastname = new ArrayList<String>();
	private List<String> dob = new ArrayList<String>();
	private List<String> street = new ArrayList<String>();
	private List<String> street_nr = new ArrayList<String>();
	private List<String> zip = new ArrayList<String>();
	private List<String> city = new ArrayList<String>();
	private List<String> country = new ArrayList<String>();
	private List<String> user_ip = new ArrayList<String>();
	private List<String> email = new ArrayList<String>();
	private List<String> cell = new ArrayList<String>();
	private List<String> phone = new ArrayList<String>();
	
	private List<String> new_pass = new ArrayList<String>();
	private List<String> new_pass_access_number = new ArrayList<String>();
	private List<String> complete_pass_id = new ArrayList<String>();
	private List<String> pass_nr = new ArrayList<String>();
	private List<String> eye_color = new ArrayList<String>();
	private List<String> body_size = new ArrayList<String>();
	private List<String> exhibition_authority = new ArrayList<String>();
	private List<String> birthname = new ArrayList<String>();
	private List<String> birthplace = new ArrayList<String>();
	private List<String> nationality = new ArrayList<String>();
	private List<String> date_of_issue = new ArrayList<String>();
	private List<String> expires_date = new ArrayList<String>();
	
	private List<String> user_data_has_change = new ArrayList<String>();
	private List<String> form_controller_action = new ArrayList<String>();
	private List<String> user_action_type = new ArrayList<String>();
	
	private List<String> transaction_id = new ArrayList<String>();
	private List<String> customer_nr = new ArrayList<String>();
	private List<String> customer_passwd = new ArrayList<String>();
	private List<String> agb_confirm = new ArrayList<String>();
	private List<String> user_agent = new ArrayList<String>();
	private String url;
 
	/**
	 *
	 * @return
	 * The dob
	 */
	public List<String> getDob() {
		return dob;
	}

	/**
	 *
	 * @param dob
	 * The dob
	 */
	public void setDob(List<String> dob) {
		this.dob = dob;
	}

	/**
	 *
	 * @return
	 * The firstname
	 */
	public List<String> getFirstname() {
		return firstname;
	}

	/**
	 *
	 * @param firstname
	 * The firstname
	 */
	public void setFirstname(List<String> firstname) {
		this.firstname = firstname;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getLastname() {
		return lastname;
	}

	public void setLastname(List<String> lastname) {
		this.lastname = lastname;
	}

	public List<String> getGender() {
		return gender;
	}

	public void setGender(List<String> gender) {
		this.gender = gender;
	}

	public List<String> getStreet() {
		return street;
	}

	public void setStreet(List<String> street) {
		this.street = street;
	}

	public List<String> getStreet_nr() {
		return street_nr;
	}

	public void setStreet_nr(List<String> street_nr) {
		this.street_nr = street_nr;
	}

	public List<String> getZip() {
		return zip;
	}

	public void setZip(List<String> zip) {
		this.zip = zip;
	}

	public List<String> getCity() {
		return city;
	}

	public void setCity(List<String> city) {
		this.city = city;
	}

	public List<String> getCountry() {
		return country;
	}

	public void setCountry(List<String> country) {
		this.country = country;
	}

	public List<String> getUser_ip() {
		return user_ip;
	}

	public void setUser_ip(List<String> user_ip) {
		this.user_ip = user_ip;
	}

	public List<String> getEmail() {
		return email;
	}

	public void setEmail(List<String> email) {
		this.email = email;
	}

	public List<String> getCell() {
		return cell;
	}

	public void setCell(List<String> cell) {
		this.cell = cell;
	}

	public List<String> getPhone() {
		return phone;
	}

	public void setPhone(List<String> phone) {
		this.phone = phone;
	}

	public List<String> getNew_pass() {
		return new_pass;
	}

	public void setNew_pass(List<String> new_pass) {
		this.new_pass = new_pass;
	}

	public List<String> getNew_pass_access_number() {
		return new_pass_access_number;
	}

	public void setNew_pass_access_number(List<String> new_pass_access_number) {
		this.new_pass_access_number = new_pass_access_number;
	}

	public List<String> getComplete_pass_id() {
		return complete_pass_id;
	}

	public void setComplete_pass_id(List<String> complete_pass_id) {
		this.complete_pass_id = complete_pass_id;
	}

	public List<String> getPass_nr() {
		return pass_nr;
	}

	public void setPass_nr(List<String> pass_nr) {
		this.pass_nr = pass_nr;
	}

	public List<String> getEye_color() {
		return eye_color;
	}

	public void setEye_color(List<String> eye_color) {
		this.eye_color = eye_color;
	}

	public List<String> getBody_size() {
		return body_size;
	}

	public void setBody_size(List<String> body_size) {
		this.body_size = body_size;
	}

	public List<String> getExhibition_authority() {
		return exhibition_authority;
	}

	public void setExhibition_authority(List<String> exhibition_authority) {
		this.exhibition_authority = exhibition_authority;
	}

	public List<String> getBirthname() {
		return birthname;
	}

	public void setBirthname(List<String> birthname) {
		this.birthname = birthname;
	}

	public List<String> getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(List<String> birthplace) {
		this.birthplace = birthplace;
	}

	public List<String> getNationality() {
		return nationality;
	}

	public void setNationality(List<String> nationality) {
		this.nationality = nationality;
	}

	public List<String> getDate_of_issue() {
		return date_of_issue;
	}

	public void setDate_of_issue(List<String> date_of_issue) {
		this.date_of_issue = date_of_issue;
	}

	public List<String> getExpires_date() {
		return expires_date;
	}

	public void setExpires_date(List<String> expires_date) {
		this.expires_date = expires_date;
	}

	public List<String> getUser_data_has_change() {
		return user_data_has_change;
	}

	public void setUser_data_has_change(List<String> user_data_has_change) {
		this.user_data_has_change = user_data_has_change;
	}

	public List<String> getForm_controller_action() {
		return form_controller_action;
	}

	public void setForm_controller_action(List<String> form_controller_action) {
		this.form_controller_action = form_controller_action;
	}

	public List<String> getUser_action_type() {
		return user_action_type;
	}

	public void setUser_action_type(List<String> user_action_type) {
		this.user_action_type = user_action_type;
	}

	public List<String> getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(List<String> transaction_id) {
		this.transaction_id = transaction_id;
	}

	public List<String> getCustomer_nr() {
		return customer_nr;
	}

	public void setCustomer_nr(List<String> customer_nr) {
		this.customer_nr = customer_nr;
	}

	public List<String> getCustomer_passwd() {
		return customer_passwd;
	}

	public void setCustomer_passwd(List<String> customer_passwd) {
		this.customer_passwd = customer_passwd;
	}

	public List<String> getAgb_confirm() {
		return agb_confirm;
	}

	public void setAgb_confirm(List<String> agb_confirm) {
		this.agb_confirm = agb_confirm;
	}

	public List<String> getUser_agent() {
		return user_agent;
	}

	public void setUser_agent(List<String> user_agent) {
		this.user_agent = user_agent;
	}

}
