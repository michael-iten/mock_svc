
package ch.axonivy.fintech.mock.dis.data;

import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class UserDataPass {

    private String exhibition_authority;
    private String expires_date;
    private String date_of_issue;
    private String pass_nr;
    private String pass_type;
    private String birthplace;
    private String birthname;
    private String nationality;

    /**
     * 
     * @return
     *     The birthplace
     */
    public String getBirthplace() {
        return birthplace;
    }

    /**
     * 
     * @param birthplace
     *     The birthplace
     */
    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    /**
     * 
     * @return
     *     The birthname
     */
    public String getBirthname() {
        return birthname;
    }

    /**
     * 
     * @param birthname
     *     The birthname
     */
    public void setBirthname(String birthname) {
        this.birthname = birthname;
    }

    /**
     * 
     * @return
     *     The nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * 
     * @param nationality
     *     The nationality
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public String getExhibition_authority() {
		return exhibition_authority;
	}

	public void setExhibition_authority(String exhibition_authority) {
		this.exhibition_authority = exhibition_authority;
	}

	public String getExpires_date() {
		return expires_date;
	}

	public void setExpires_date(String expires_date) {
		this.expires_date = expires_date;
	}

	public String getDate_of_issue() {
		return date_of_issue;
	}

	public void setDate_of_issue(String date_of_issue) {
		this.date_of_issue = date_of_issue;
	}

	public String getPass_nr() {
		return pass_nr;
	}

	public void setPass_nr(String pass_nr) {
		this.pass_nr = pass_nr;
	}

	public String getPass_type() {
		return pass_type;
	}

	public void setPass_type(String pass_type) {
		this.pass_type = pass_type;
	}

}
