
package ch.axonivy.fintech.mock.dis.data;

import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class WebidResults {

    private webid_result webid_result;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


	public webid_result getWebid_result() {
		return webid_result;
	}


	public void setWebid_result(webid_result webid_result) {
		this.webid_result = webid_result;
	}

}
