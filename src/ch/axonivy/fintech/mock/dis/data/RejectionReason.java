package ch.axonivy.fintech.mock.dis.data;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class RejectionReason {

	private String reason_name;

	public String getReason_name() {
		return reason_name;
	}

	public void setReason_name(String reason_name) {
		this.reason_name = reason_name;
	}
}
