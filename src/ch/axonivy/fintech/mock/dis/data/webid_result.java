
package ch.axonivy.fintech.mock.dis.data;

import javax.annotation.Generated;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class webid_result {

    private String confirmed;
    private String doc_signed;
    private String transaction_id;
    private String webid_action_id;
    private UserData user_data;
    private UserDataPass user_data_pass;
    private CallcenterData callcenter_data;
    private RejectionReason rejection_reason;

    /**
     * 
     * @return
     *     The confirmed
     */
    public String getConfirmed() {
        return confirmed;
    }

    /**
     * 
     * @param confirmed
     *     The confirmed
     */
    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getWebid_action_id() {
		return webid_action_id;
	}

	public void setWebid_action_id(String webid_action_id) {
		this.webid_action_id = webid_action_id;
	}

	public UserData getUser_data() {
		return user_data;
	}

	public void setUser_data(UserData user_data) {
		this.user_data = user_data;
	}

	public UserDataPass getUser_data_pass() {
		return user_data_pass;
	}

	public void setUser_data_pass(UserDataPass user_data_pass) {
		this.user_data_pass = user_data_pass;
	}

	public CallcenterData getCallcenter_data() {
		return callcenter_data;
	}

	public void setCallcenter_data(CallcenterData callcenter_data) {
		this.callcenter_data = callcenter_data;
	}

	public String getDoc_signed() {
		return doc_signed;
	}

	public void setDoc_signed(String doc_signed) {
		this.doc_signed = doc_signed;
	}

	public RejectionReason getRejection_reason() {
		return rejection_reason;
	}

	public void setRejection_reason(RejectionReason rejection_reason) {
		this.rejection_reason = rejection_reason;
	}

}
