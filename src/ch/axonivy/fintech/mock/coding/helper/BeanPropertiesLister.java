package ch.axonivy.fintech.mock.coding.helper;

import java.lang.reflect.Field;

import ch.ivyteam.ivy.environment.Ivy;


public class BeanPropertiesLister {
	
	public static void listPropertiesOf(Class<?> clazz) {
		String output = "";
		for (Field field : clazz.getDeclaredFields()) {
			if (!field.getName().isEmpty()) {
				if(!output.isEmpty()) {
					output += ",";
				}
				field.setAccessible(true);
				output += field.getGenericType().getTypeName().toString() + " " + field.getName();
				
			}
		}
		
		Ivy.log().info(output);
		
	}
	
	public static void listPropertiesForQueryParamsOutputOf(Class<?> clazz) {
		String output = "";
		for (Field field : clazz.getDeclaredFields()) {
			if (!field.getName().isEmpty()) {
				if(!output.isEmpty()) {
					output += ",";
				}
				field.setAccessible(true);
				output += "@QueryParam(\"" + field.getName() + "\") " + field.getGenericType().getTypeName().toString() + " " + field.getName();
				
			}
		}
		
		Ivy.log().info(output);
		
	}

}
