[Ivy]
162C49391B318D5E 3.20 #module
>Proto >Proto Collection #zClass
ds0 DisVideoIdentificationProcess Big #zClass
ds0 RD #cInfo
ds0 #process
ds0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
ds0 @TextInP .rdData2UIAction .rdData2UIAction #zField
ds0 @TextInP .resExport .resExport #zField
ds0 @TextInP .type .type #zField
ds0 @TextInP .processKind .processKind #zField
ds0 @AnnotationInP-0n ai ai #zField
ds0 @MessageFlowInP-0n messageIn messageIn #zField
ds0 @MessageFlowOutP-0n messageOut messageOut #zField
ds0 @TextInP .xml .xml #zField
ds0 @TextInP .responsibility .responsibility #zField
ds0 @RichDialogInitStart f0 '' #zField
ds0 @RichDialogProcessEnd f1 '' #zField
ds0 @RichDialogProcessStart f3 '' #zField
ds0 @RichDialogEnd f4 '' #zField
ds0 @GridStep f6 '' #zField
ds0 @PushWFArc f7 '' #zField
ds0 @PushWFArc f2 '' #zField
ds0 @RichDialogProcessStart f8 '' #zField
ds0 @GridStep f9 '' #zField
ds0 @PushWFArc f10 '' #zField
ds0 @RichDialogProcessEnd f11 '' #zField
ds0 @PushWFArc f12 '' #zField
ds0 @RichDialogProcessStart f13 '' #zField
ds0 @GridStep f14 '' #zField
ds0 @PushWFArc f15 '' #zField
ds0 @RichDialogProcessEnd f16 '' #zField
ds0 @PushWFArc f17 '' #zField
ds0 @GridStep f18 '' #zField
ds0 @PushWFArc f19 '' #zField
ds0 @RestClientCall f20 '' #zField
ds0 @PushWFArc f21 '' #zField
ds0 @ErrorBoundaryEvent f22 '' #zField
ds0 @RichDialogProcessEnd f23 '' #zField
ds0 @PushWFArc f24 '' #zField
ds0 @Trigger f27 '' #zField
ds0 @PushWFArc f28 '' #zField
ds0 @PushWFArc f5 '' #zField
ds0 @RichDialogProcessStart f25 '' #zField
ds0 @RichDialogProcessEnd f26 '' #zField
ds0 @PushWFArc f29 '' #zField
>Proto ds0 ds0 DisVideoIdentificationProcess #zField
ds0 f0 guid 162C49392CC4D664 #txt
ds0 f0 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f0 method start(fintech.external.services.mock.SignPerson,String) #txt
ds0 f0 disableUIEvents true #txt
ds0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<fintech.external.services.mock.SignPerson signPerson,java.lang.String cobId> param = methodEvent.getInputArguments();
' #txt
ds0 f0 inParameterMapAction 'out.cobId=param.cobId;
out.signPerson=param.signPerson;
' #txt
ds0 f0 outParameterDecl '<> result;
' #txt
ds0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f0 83 51 26 26 -16 15 #rect
ds0 f0 @|RichDialogInitStartIcon #fIcon
ds0 f1 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f1 395 51 26 26 0 12 #rect
ds0 f1 @|RichDialogProcessEndIcon #fIcon
ds0 f3 guid 162C49392F36DFAF #txt
ds0 f3 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f3 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f3 actionTable 'out=in;
' #txt
ds0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
ds0 f3 83 147 26 26 -15 12 #rect
ds0 f3 @|RichDialogProcessStartIcon #fIcon
ds0 f4 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f4 guid 162C49392F45E173 #txt
ds0 f4 947 147 26 26 0 12 #rect
ds0 f4 @|RichDialogEndIcon #fIcon
ds0 f6 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f6 actionTable 'out=in;
out.isDocumentSigned="yes";
out.responseSelected="success";
out.successSelected=true;
' #txt
ds0 f6 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f6 208 42 112 44 -8 -8 #rect
ds0 f6 @|StepIcon #fIcon
ds0 f7 expr out #txt
ds0 f7 109 64 208 64 #arcP
ds0 f2 expr out #txt
ds0 f2 320 64 395 64 #arcP
ds0 f8 guid 162D3E1E0725CC85 #txt
ds0 f8 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f8 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f8 actionTable 'out=in;
' #txt
ds0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>resultSelectionChanged</name>
        <nameStyle>22,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f8 83 243 26 26 -66 15 #rect
ds0 f8 @|RichDialogProcessStartIcon #fIcon
ds0 f9 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f9 actionTable 'out=in;
' #txt
ds0 f9 actionCode 'ivy.log.info(in.responseSelected);
out.rejectionSelected = in.responseSelected.equalsIgnoreCase("rejection");
out.successSelected = in.responseSelected.equalsIgnoreCase("success");' #txt
ds0 f9 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f9 192 234 112 44 0 -8 #rect
ds0 f9 @|StepIcon #fIcon
ds0 f10 expr out #txt
ds0 f10 109 256 192 256 #arcP
ds0 f11 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f11 371 243 26 26 0 12 #rect
ds0 f11 @|RichDialogProcessEndIcon #fIcon
ds0 f12 expr out #txt
ds0 f12 304 256 371 256 #arcP
ds0 f13 guid 162D435ED51FBB85 #txt
ds0 f13 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f13 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f13 actionTable 'out=in;
' #txt
ds0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reasonSelected</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f13 83 339 26 26 -43 15 #rect
ds0 f13 @|RichDialogProcessStartIcon #fIcon
ds0 f14 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f14 actionTable 'out=in;
' #txt
ds0 f14 actionCode ivy.log.info(in.reasonSelected); #txt
ds0 f14 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f14 192 330 112 44 0 -8 #rect
ds0 f14 @|StepIcon #fIcon
ds0 f15 expr out #txt
ds0 f15 109 352 192 352 #arcP
ds0 f16 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f16 371 339 26 26 0 12 #rect
ds0 f16 @|RichDialogProcessEndIcon #fIcon
ds0 f17 expr out #txt
ds0 f17 304 352 371 352 #arcP
ds0 f18 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f18 actionTable 'out=in;
out.webIdResults.webid_result.confirmed=in.successSelected ? "1" : "0";
out.webIdResults.webid_result.doc_signed=in.isDocumentSigned.equalsIgnoreCase("yes") ? "1" : "0";
out.webIdResults.webid_result.rejection_reason.reason_name=in.reasonSelected;
out.webIdResults.webid_result.transaction_id=in.cobId;
out.webIdResults.webid_result.user_data.city=in.signPerson.city;
out.webIdResults.webid_result.user_data.country=in.signPerson.country;
out.webIdResults.webid_result.user_data.dob=in.signPerson.dob;
out.webIdResults.webid_result.user_data.firstname=in.signPerson.firstname;
out.webIdResults.webid_result.user_data.lastname=in.signPerson.lastname;
out.webIdResults.webid_result.user_data.street=in.signPerson.street;
out.webIdResults.webid_result.user_data.street_nr=in.signPerson.street_nr;
out.webIdResults.webid_result.user_data.zip=in.signPerson.zip;
out.webIdResults.webid_result.user_data_pass.birthname=in.signPerson.birthname;
out.webIdResults.webid_result.user_data_pass.birthplace=in.signPerson.birthplace;
out.webIdResults.webid_result.user_data_pass.exhibition_authority=in.signPerson.exhibition_authority;
out.webIdResults.webid_result.user_data_pass.expires_date=in.signPerson.expires_date;
out.webIdResults.webid_result.user_data_pass.nationality=in.signPerson.nationality;
out.webIdResults.webid_result.user_data_pass.pass_nr=in.signPerson.pass_nr;
' #txt
ds0 f18 actionCode 'ivy.log.debug("[DIS MOCK {0}] proceeding DIS", in.cobId);' #txt
ds0 f18 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f18 176 138 112 44 0 -8 #rect
ds0 f18 @|StepIcon #fIcon
ds0 f19 expr out #txt
ds0 f19 109 160 176 160 #arcP
ds0 f20 clientId ac414fe9-fdb7-422f-8958-5486506df073 #txt
ds0 f20 headers 'Accept=*/*;
X-Requested-By=ivy;
' #txt
ds0 f20 method POST #txt
ds0 f20 bodyInputType FORM #txt
ds0 f20 bodyMediaType application/x-www-form-urlencoded #txt
ds0 f20 bodyForm 'webid_response=ch.axonivy.fintech.mock.dis.WebIdResultsProducer.produceBase64Result(in.webIdResults);
your_transaction_id=in.webIdResults.webid_result.transaction_id;
webid_action_id=in.webIdResults.webid_result.transaction_id;
webid_confirmed=in.webIdResults.webid_result.confirmed;
webid_doc_signed=in.webIdResults.webid_result.doc_signed;
' #txt
ds0 f20 resultType java.lang.String #txt
ds0 f20 responseCode ivy.log.debug(result); #txt
ds0 f20 clientErrorCode ivy:error:rest:client #txt
ds0 f20 statusErrorCode ivy:error:rest:client #txt
ds0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call WebIDService in Fintech Standard</name>
    </language>
</elementInfo>
' #txt
ds0 f20 392 138 224 44 -104 -8 #rect
ds0 f20 @|RestClientCallIcon #fIcon
ds0 f21 expr out #txt
ds0 f21 288 160 392 160 #arcP
ds0 f22 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f22 actionTable 'out=in;
' #txt
ds0 f22 actionCode 'ivy.log.error("Error while calling webIdService " + error.getErrorMessage(), error.getCause());' #txt
ds0 f22 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f22 attachedToRef 162C49391B318D5E-f20 #txt
ds0 f22 577 177 30 30 0 15 #rect
ds0 f22 @|ErrorBoundaryEventIcon #fIcon
ds0 f23 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f23 691 275 26 26 0 12 #rect
ds0 f23 @|RichDialogProcessEndIcon #fIcon
ds0 f24 592 207 691 288 #arcP
ds0 f24 1 592 288 #addKink
ds0 f24 1 0.07179773830652328 0 0 #arcLabel
ds0 f27 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f27 processCall 'Functional Processes/dis/generateDoc:start(String)' #txt
ds0 f27 doCall true #txt
ds0 f27 requestActionDecl '<java.lang.String cobId> param;
' #txt
ds0 f27 requestMappingAction 'param.cobId=in.cobId;
' #txt
ds0 f27 responseActionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f27 responseMappingAction 'out=in;
' #txt
ds0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>generateDoc</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f27 720 138 112 44 -35 -8 #rect
ds0 f27 @|TriggerIcon #fIcon
ds0 f28 616 160 720 160 #arcP
ds0 f5 expr out #txt
ds0 f5 832 160 947 160 #arcP
ds0 f25 guid 162E31BC0A5B4A05 #txt
ds0 f25 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f25 actionDecl 'fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData out;
' #txt
ds0 f25 actionTable 'out=in;
' #txt
ds0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>documentSigned</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ds0 f25 83 435 26 26 -47 15 #rect
ds0 f25 @|RichDialogProcessStartIcon #fIcon
ds0 f26 type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
ds0 f26 211 435 26 26 0 12 #rect
ds0 f26 @|RichDialogProcessEndIcon #fIcon
ds0 f29 expr out #txt
ds0 f29 109 448 211 448 #arcP
>Proto ds0 .type fintech.external.services.mock.DisVideoIdentification.DisVideoIdentificationData #txt
>Proto ds0 .processKind HTML_DIALOG #txt
>Proto ds0 -8 -8 16 16 16 26 #rect
>Proto ds0 '' #fIcon
ds0 f0 mainOut f7 tail #connect
ds0 f7 head f6 mainIn #connect
ds0 f6 mainOut f2 tail #connect
ds0 f2 head f1 mainIn #connect
ds0 f8 mainOut f10 tail #connect
ds0 f10 head f9 mainIn #connect
ds0 f9 mainOut f12 tail #connect
ds0 f12 head f11 mainIn #connect
ds0 f13 mainOut f15 tail #connect
ds0 f15 head f14 mainIn #connect
ds0 f14 mainOut f17 tail #connect
ds0 f17 head f16 mainIn #connect
ds0 f3 mainOut f19 tail #connect
ds0 f19 head f18 mainIn #connect
ds0 f18 mainOut f21 tail #connect
ds0 f21 head f20 mainIn #connect
ds0 f22 mainOut f24 tail #connect
ds0 f24 head f23 mainIn #connect
ds0 f20 mainOut f28 tail #connect
ds0 f28 head f27 mainIn #connect
ds0 f27 mainOut f5 tail #connect
ds0 f5 head f4 mainIn #connect
ds0 f25 mainOut f29 tail #connect
ds0 f29 head f26 mainIn #connect
