[Ivy]
162D3E61C8D701BE 3.20 #module
>Proto >Proto Collection #zClass
tt0 test Big #zClass
tt0 B #cInfo
tt0 #process
tt0 @TextInP .resExport .resExport #zField
tt0 @TextInP .type .type #zField
tt0 @TextInP .processKind .processKind #zField
tt0 @AnnotationInP-0n ai ai #zField
tt0 @MessageFlowInP-0n messageIn messageIn #zField
tt0 @MessageFlowOutP-0n messageOut messageOut #zField
tt0 @TextInP .xml .xml #zField
tt0 @TextInP .responsibility .responsibility #zField
tt0 @StartRequest f0 '' #zField
tt0 @EndTask f1 '' #zField
tt0 @RichDialog f3 '' #zField
tt0 @PushWFArc f2 '' #zField
tt0 @GridStep f5 '' #zField
tt0 @PushWFArc f6 '' #zField
tt0 @PushWFArc f4 '' #zField
tt0 @StartRequest f7 '' #zField
tt0 @GridStep f8 '' #zField
tt0 @PushWFArc f9 '' #zField
tt0 @EndTask f10 '' #zField
tt0 @PushWFArc f11 '' #zField
tt0 @StartRequest f12 '' #zField
tt0 @GridStep f13 '' #zField
tt0 @PushWFArc f14 '' #zField
tt0 @EndTask f15 '' #zField
tt0 @PushWFArc f16 '' #zField
>Proto tt0 tt0 test #zField
tt0 f0 outLink start.ivp #txt
tt0 f0 type fintech.external.services.mock.Data #txt
tt0 f0 inParamDecl '<> param;' #txt
tt0 f0 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f0 guid 162D3E61C9DF179D #txt
tt0 f0 requestEnabled true #txt
tt0 f0 triggerEnabled false #txt
tt0 f0 callSignature start() #txt
tt0 f0 caseData businessCase.attach=true #txt
tt0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
tt0 f0 @C|.responsibility Everybody #txt
tt0 f0 81 49 30 30 -21 17 #rect
tt0 f0 @|StartRequestIcon #fIcon
tt0 f1 type fintech.external.services.mock.Data #txt
tt0 f1 497 49 30 30 0 15 #rect
tt0 f1 @|EndIcon #fIcon
tt0 f3 targetWindow NEW #txt
tt0 f3 targetDisplay TOP #txt
tt0 f3 richDialogId fintech.external.services.mock.DisVideoIdentification #txt
tt0 f3 startMethod start(fintech.external.services.mock.SignPerson,String) #txt
tt0 f3 type fintech.external.services.mock.Data #txt
tt0 f3 requestActionDecl '<fintech.external.services.mock.SignPerson signPerson, String cobId> param;' #txt
tt0 f3 requestMappingAction 'param.signPerson.birthname="Comba";
param.signPerson.cell="+41 78 589 56 52";
param.signPerson.city="Zürich";
param.signPerson.country="CH";
param.signPerson.dob="01.01.1990";
param.signPerson.firstname="Emmanuel";
param.signPerson.gender="Herr";
param.signPerson.lastname="Comba";
param.signPerson.nationality="CH";
param.signPerson.street="Teststrasse";
param.signPerson.street_nr="45";
param.signPerson.transaction_id="COB000001-0";
param.signPerson.zip="8001";
param.cobId="COB000001-0";
' #txt
tt0 f3 responseActionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f3 responseMappingAction 'out=in;
' #txt
tt0 f3 isAsynch false #txt
tt0 f3 isInnerRd false #txt
tt0 f3 userContext '* ' #txt
tt0 f3 328 42 112 44 0 -8 #rect
tt0 f3 @|RichDialogIcon #fIcon
tt0 f2 expr out #txt
tt0 f2 440 64 497 64 #arcP
tt0 f5 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f5 actionTable 'out=in;
' #txt
tt0 f5 actionCode 'ivy.log.info("application name {0}", ivy.wf.getApplication().getName());' #txt
tt0 f5 type fintech.external.services.mock.Data #txt
tt0 f5 168 42 112 44 0 -8 #rect
tt0 f5 @|StepIcon #fIcon
tt0 f6 expr out #txt
tt0 f6 111 64 168 64 #arcP
tt0 f4 expr out #txt
tt0 f4 280 64 328 64 #arcP
tt0 f7 outLink start2.ivp #txt
tt0 f7 type fintech.external.services.mock.Data #txt
tt0 f7 inParamDecl '<> param;' #txt
tt0 f7 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f7 guid 162D7B7C35DB8731 #txt
tt0 f7 requestEnabled true #txt
tt0 f7 triggerEnabled false #txt
tt0 f7 callSignature start2() #txt
tt0 f7 caseData businessCase.attach=true #txt
tt0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start2.ivp</name>
    </language>
</elementInfo>
' #txt
tt0 f7 @C|.responsibility Everybody #txt
tt0 f7 81 209 30 30 -24 17 #rect
tt0 f7 @|StartRequestIcon #fIcon
tt0 f8 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f8 actionTable 'out=in;
' #txt
tt0 f8 actionCode 'import fintech.external.services.mock.SignPerson;
import ch.axonivy.fintech.mock.coding.helper.BeanPropertiesLister;
BeanPropertiesLister.listPropertiesForQueryParamsOutputOf(SignPerson.class);' #txt
tt0 f8 type fintech.external.services.mock.Data #txt
tt0 f8 168 202 112 44 0 -8 #rect
tt0 f8 @|StepIcon #fIcon
tt0 f9 expr out #txt
tt0 f9 111 224 168 224 #arcP
tt0 f10 type fintech.external.services.mock.Data #txt
tt0 f10 353 209 30 30 0 15 #rect
tt0 f10 @|EndIcon #fIcon
tt0 f11 expr out #txt
tt0 f11 280 224 353 224 #arcP
tt0 f12 outLink start3.ivp #txt
tt0 f12 type fintech.external.services.mock.Data #txt
tt0 f12 inParamDecl '<> param;' #txt
tt0 f12 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f12 guid 162DF664E3BEB0C3 #txt
tt0 f12 requestEnabled true #txt
tt0 f12 triggerEnabled false #txt
tt0 f12 callSignature start3() #txt
tt0 f12 caseData businessCase.attach=true #txt
tt0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start3.ivp</name>
    </language>
</elementInfo>
' #txt
tt0 f12 @C|.responsibility Everybody #txt
tt0 f12 81 353 30 30 -24 17 #rect
tt0 f12 @|StartRequestIcon #fIcon
tt0 f13 actionDecl 'fintech.external.services.mock.Data out;
' #txt
tt0 f13 actionTable 'out=in;
' #txt
tt0 f13 actionCode 'import ch.axonivy.fintech.mock.dis.file.DisDocumentsPackageHandler;
import org.apache.commons.io.FileUtils;

DateTime dt = new DateTime();

File origin = new File("DISMOCK.zip");
ivy.log.debug("File {0} found {1}",origin, origin.exists());
String fileName = "DISMOCK_00125__177672700_" + "COB00001-1" + "_" + dt.toNumber() + ".zip";
ivy.wf.getApplication().getFileArea().getParentFile();
ivy.log.debug("Destination {0} ",ivy.wf.getApplication().getFileArea().getParentFile().getAbsolutePath());


DisDocumentsPackageHandler ddh = new DisDocumentsPackageHandler();' #txt
tt0 f13 type fintech.external.services.mock.Data #txt
tt0 f13 152 346 112 44 0 -8 #rect
tt0 f13 @|StepIcon #fIcon
tt0 f14 expr out #txt
tt0 f14 111 368 152 368 #arcP
tt0 f15 type fintech.external.services.mock.Data #txt
tt0 f15 369 353 30 30 0 15 #rect
tt0 f15 @|EndIcon #fIcon
tt0 f16 expr out #txt
tt0 f16 264 368 369 368 #arcP
>Proto tt0 .type fintech.external.services.mock.Data #txt
>Proto tt0 .processKind NORMAL #txt
>Proto tt0 0 0 32 24 18 0 #rect
>Proto tt0 @|BIcon #fIcon
tt0 f3 mainOut f2 tail #connect
tt0 f2 head f1 mainIn #connect
tt0 f0 mainOut f6 tail #connect
tt0 f6 head f5 mainIn #connect
tt0 f5 mainOut f4 tail #connect
tt0 f4 head f3 mainIn #connect
tt0 f7 mainOut f9 tail #connect
tt0 f9 head f8 mainIn #connect
tt0 f8 mainOut f11 tail #connect
tt0 f11 head f10 mainIn #connect
tt0 f12 mainOut f14 tail #connect
tt0 f14 head f13 mainIn #connect
tt0 f13 mainOut f16 tail #connect
tt0 f16 head f15 mainIn #connect
