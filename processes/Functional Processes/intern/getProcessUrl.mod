[Ivy]
162DDD28AD3A82E1 3.20 #module
>Proto >Proto Collection #zClass
gl0 getProcessUrl Big #zClass
gl0 B #cInfo
gl0 #process
gl0 @TextInP .resExport .resExport #zField
gl0 @TextInP .type .type #zField
gl0 @TextInP .processKind .processKind #zField
gl0 @AnnotationInP-0n ai ai #zField
gl0 @MessageFlowInP-0n messageIn messageIn #zField
gl0 @MessageFlowOutP-0n messageOut messageOut #zField
gl0 @TextInP .xml .xml #zField
gl0 @TextInP .responsibility .responsibility #zField
gl0 @StartSub f0 '' #zField
gl0 @EndSub f1 '' #zField
gl0 @GridStep f3 '' #zField
gl0 @PushWFArc f4 '' #zField
gl0 @PushWFArc f2 '' #zField
>Proto gl0 gl0 getProcessUrl #zField
gl0 f0 inParamDecl '<java.lang.String processName> param;' #txt
gl0 f0 inParamTable 'out.processName=param.processName;
' #txt
gl0 f0 outParamDecl '<java.lang.String processUrl> result;
' #txt
gl0 f0 outParamTable 'result.processUrl=in.url;
' #txt
gl0 f0 actionDecl 'fintech.external.services.mock.GetProcessUrlData out;
' #txt
gl0 f0 callSignature call(String) #txt
gl0 f0 type fintech.external.services.mock.GetProcessUrlData #txt
gl0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(String)</name>
    </language>
</elementInfo>
' #txt
gl0 f0 81 49 30 30 -29 17 #rect
gl0 f0 @|StartSubIcon #fIcon
gl0 f1 type fintech.external.services.mock.GetProcessUrlData #txt
gl0 f1 337 49 30 30 0 15 #rect
gl0 f1 @|EndSubIcon #fIcon
gl0 f3 actionDecl 'fintech.external.services.mock.GetProcessUrlData out;
' #txt
gl0 f3 actionTable 'out=in;
' #txt
gl0 f3 actionCode 'out.url = ivy.html.startref(in.processName);' #txt
gl0 f3 type fintech.external.services.mock.GetProcessUrlData #txt
gl0 f3 168 42 112 44 0 -8 #rect
gl0 f3 @|StepIcon #fIcon
gl0 f4 expr out #txt
gl0 f4 111 64 168 64 #arcP
gl0 f2 expr out #txt
gl0 f2 280 64 337 64 #arcP
>Proto gl0 .type fintech.external.services.mock.GetProcessUrlData #txt
>Proto gl0 .processKind CALLABLE_SUB #txt
>Proto gl0 0 0 32 24 18 0 #rect
>Proto gl0 @|BIcon #fIcon
gl0 f0 mainOut f4 tail #connect
gl0 f4 head f3 mainIn #connect
gl0 f3 mainOut f2 tail #connect
gl0 f2 head f1 mainIn #connect
