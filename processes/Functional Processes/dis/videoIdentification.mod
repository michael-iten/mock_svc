[Ivy]
162D9B06E7844FCB 3.20 #module
>Proto >Proto Collection #zClass
vn0 videoIdentification Big #zClass
vn0 B #cInfo
vn0 #process
vn0 @TextInP .resExport .resExport #zField
vn0 @TextInP .type .type #zField
vn0 @TextInP .processKind .processKind #zField
vn0 @AnnotationInP-0n ai ai #zField
vn0 @MessageFlowInP-0n messageIn messageIn #zField
vn0 @MessageFlowOutP-0n messageOut messageOut #zField
vn0 @TextInP .xml .xml #zField
vn0 @TextInP .responsibility .responsibility #zField
vn0 @StartRequest f0 '' #zField
vn0 @EndTask f1 '' #zField
vn0 @GridStep f3 '' #zField
vn0 @PushWFArc f4 '' #zField
vn0 @RichDialog f5 '' #zField
vn0 @PushWFArc f6 '' #zField
vn0 @PushWFArc f2 '' #zField
>Proto vn0 vn0 videoIdentification #zField
vn0 f0 outLink videoIdentification.ivp #txt
vn0 f0 type fintech.external.services.mock.VideoIdentificationData #txt
vn0 f0 inParamDecl '<java.lang.String cell,java.lang.String city,java.lang.String user_data_has_change,java.lang.String form_controller_action,java.lang.String user_action_type,java.lang.String transaction_id,java.lang.String customer_nr,java.lang.String customer_passwd,java.lang.String agb_confirm,java.lang.String birthplace,java.lang.String nationality,java.lang.String gender,java.lang.String firstname,java.lang.String lastname,java.lang.String dob,java.lang.String street,java.lang.String street_nr,java.lang.String zip,java.lang.String country,java.lang.String user_ip,java.lang.String email> param;' #txt
vn0 f0 inParamTable 'out.cobId=param.transaction_id;
out.signPerson.agb_confirm=param.agb_confirm;
out.signPerson.birthplace=param.birthplace;
out.signPerson.cell=param.cell;
out.signPerson.city=param.city;
out.signPerson.country=param.country;
out.signPerson.customer_nr=param.customer_nr;
out.signPerson.customer_passwd=param.customer_passwd;
out.signPerson.dob=param.dob;
out.signPerson.email=param.email;
out.signPerson.firstname=param.firstname;
out.signPerson.form_controller_action=param.form_controller_action;
out.signPerson.gender=param.gender;
out.signPerson.lastname=param.lastname;
out.signPerson.nationality=param.nationality;
out.signPerson.street=param.street;
out.signPerson.street_nr=param.street_nr;
out.signPerson.transaction_id=param.transaction_id;
out.signPerson.user_action_type=param.user_action_type;
out.signPerson.user_data_has_change="1".equals(param.user_data_has_change);
out.signPerson.user_ip=param.user_ip;
out.signPerson.zip=param.zip;
' #txt
vn0 f0 actionDecl 'fintech.external.services.mock.VideoIdentificationData out;
' #txt
vn0 f0 guid 162D9B06E7F7AF96 #txt
vn0 f0 requestEnabled true #txt
vn0 f0 triggerEnabled false #txt
vn0 f0 callSignature videoIdentification(String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String) #txt
vn0 f0 persist false #txt
vn0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
vn0 f0 caseData businessCase.attach=true #txt
vn0 f0 showInStartList 1 #txt
vn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>videoIdentification.ivp</name>
        <nameStyle>23,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
vn0 f0 @C|.responsibility Everybody #txt
vn0 f0 73 89 30 30 -58 17 #rect
vn0 f0 @|StartRequestIcon #fIcon
vn0 f1 type fintech.external.services.mock.VideoIdentificationData #txt
vn0 f1 521 89 30 30 0 15 #rect
vn0 f1 @|EndIcon #fIcon
vn0 f3 actionDecl 'fintech.external.services.mock.VideoIdentificationData out;
' #txt
vn0 f3 actionTable 'out=in;
' #txt
vn0 f3 actionCode 'import java.net.URLDecoder;
//ivy.wf.findProcessStartsBySignature("videoIdentification()").isEmpty()
//ivy.log.debug(ivy.html.startref("Functional Processes/dis/videoIdentification/videoIdentification.ivp"));URLDecoder

ivy.log.info(URLDecoder.decode(in.signPerson.city,"UTF-8"));' #txt
vn0 f3 type fintech.external.services.mock.VideoIdentificationData #txt
vn0 f3 168 82 112 44 0 -8 #rect
vn0 f3 @|StepIcon #fIcon
vn0 f4 expr out #txt
vn0 f4 103 104 168 104 #arcP
vn0 f5 targetWindow NEW #txt
vn0 f5 targetDisplay TOP #txt
vn0 f5 richDialogId fintech.external.services.mock.DisVideoIdentification #txt
vn0 f5 startMethod start(fintech.external.services.mock.SignPerson,String) #txt
vn0 f5 type fintech.external.services.mock.VideoIdentificationData #txt
vn0 f5 requestActionDecl '<fintech.external.services.mock.SignPerson signPerson, String cobId> param;' #txt
vn0 f5 requestMappingAction 'param.signPerson=in.signPerson;
param.cobId=in.cobId;
' #txt
vn0 f5 responseActionDecl 'fintech.external.services.mock.VideoIdentificationData out;
' #txt
vn0 f5 responseMappingAction 'out=in;
' #txt
vn0 f5 isAsynch false #txt
vn0 f5 isInnerRd false #txt
vn0 f5 userContext '* ' #txt
vn0 f5 352 82 112 44 0 -8 #rect
vn0 f5 @|RichDialogIcon #fIcon
vn0 f6 expr out #txt
vn0 f6 280 104 352 104 #arcP
vn0 f2 expr out #txt
vn0 f2 464 104 521 104 #arcP
>Proto vn0 .type fintech.external.services.mock.VideoIdentificationData #txt
>Proto vn0 .processKind NORMAL #txt
>Proto vn0 0 0 32 24 18 0 #rect
>Proto vn0 @|BIcon #fIcon
vn0 f0 mainOut f4 tail #connect
vn0 f4 head f3 mainIn #connect
vn0 f3 mainOut f6 tail #connect
vn0 f6 head f5 mainIn #connect
vn0 f5 mainOut f2 tail #connect
vn0 f2 head f1 mainIn #connect
