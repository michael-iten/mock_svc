[Ivy]
162DED7F465379BC 3.20 #module
>Proto >Proto Collection #zClass
gc0 generateDoc Big #zClass
gc0 B #cInfo
gc0 #process
gc0 @TextInP .resExport .resExport #zField
gc0 @TextInP .type .type #zField
gc0 @TextInP .processKind .processKind #zField
gc0 @AnnotationInP-0n ai ai #zField
gc0 @MessageFlowInP-0n messageIn messageIn #zField
gc0 @MessageFlowOutP-0n messageOut messageOut #zField
gc0 @TextInP .xml .xml #zField
gc0 @TextInP .responsibility .responsibility #zField
gc0 @StartRequest f0 '' #zField
gc0 @EndTask f1 '' #zField
gc0 @GridStep f5 '' #zField
gc0 @PushWFArc f2 '' #zField
gc0 @ErrorBoundaryEvent f7 '' #zField
gc0 @EndTask f10 '' #zField
gc0 @PushWFArc f12 '' #zField
gc0 @PushWFArc f8 '' #zField
>Proto gc0 gc0 generateDoc #zField
gc0 f0 outLink start.ivp #txt
gc0 f0 type fintech.external.services.mock.GenerateDocData #txt
gc0 f0 inParamDecl '<java.lang.String cobId> param;' #txt
gc0 f0 inParamTable 'out.cobId=param.cobId;
' #txt
gc0 f0 actionDecl 'fintech.external.services.mock.GenerateDocData out;
' #txt
gc0 f0 guid 162DED7F46D58597 #txt
gc0 f0 requestEnabled true #txt
gc0 f0 triggerEnabled true #txt
gc0 f0 callSignature start(String) #txt
gc0 f0 persist false #txt
gc0 f0 taskData 'TaskTriggered.ROL=SYSTEM
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
gc0 f0 caseData businessCase.attach=true #txt
gc0 f0 showInStartList 1 #txt
gc0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp
start(String)</name>
    </language>
</elementInfo>
' #txt
gc0 f0 @C|.responsibility Everybody #txt
gc0 f0 81 49 30 30 -32 17 #rect
gc0 f0 @|StartRequestIcon #fIcon
gc0 f1 type fintech.external.services.mock.GenerateDocData #txt
gc0 f1 449 49 30 30 0 15 #rect
gc0 f1 @|EndIcon #fIcon
gc0 f5 actionDecl 'fintech.external.services.mock.GenerateDocData out;
' #txt
gc0 f5 actionTable 'out=in;
' #txt
gc0 f5 actionCode 'import ch.axonivy.fintech.mock.dis.file.DisDocumentsPackageHandler;
DisDocumentsPackageHandler ddh = new DisDocumentsPackageHandler();

ddh.sendDisZipPackageForDossier(in.cobId);' #txt
gc0 f5 type fintech.external.services.mock.GenerateDocData #txt
gc0 f5 232 42 112 44 0 -8 #rect
gc0 f5 @|StepIcon #fIcon
gc0 f2 expr out #txt
gc0 f2 344 64 449 64 #arcP
gc0 f7 actionDecl 'fintech.external.services.mock.GenerateDocData out;
' #txt
gc0 f7 actionTable 'out=in;
' #txt
gc0 f7 actionCode 'ivy.log.error(error.getMessage(), error.getCause());' #txt
gc0 f7 type fintech.external.services.mock.GenerateDocData #txt
gc0 f7 attachedToRef 162DED7F465379BC-f5 #txt
gc0 f7 305 81 30 30 0 15 #rect
gc0 f7 @|ErrorBoundaryEventIcon #fIcon
gc0 f10 type fintech.external.services.mock.GenerateDocData #txt
gc0 f10 457 145 30 30 0 15 #rect
gc0 f10 @|EndIcon #fIcon
gc0 f12 333 101 458 154 #arcP
gc0 f8 expr out #txt
gc0 f8 111 64 232 64 #arcP
>Proto gc0 .type fintech.external.services.mock.GenerateDocData #txt
>Proto gc0 .processKind NORMAL #txt
>Proto gc0 0 0 32 24 18 0 #rect
>Proto gc0 @|BIcon #fIcon
gc0 f5 mainOut f2 tail #connect
gc0 f2 head f1 mainIn #connect
gc0 f7 mainOut f12 tail #connect
gc0 f12 head f10 mainIn #connect
gc0 f0 mainOut f8 tail #connect
gc0 f8 head f5 mainIn #connect
