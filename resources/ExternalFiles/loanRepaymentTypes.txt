{
  "id" : "RepaymentTypes",
  "name" : "Repayment type",
  "items" : [ {
    "code" : "2000",
    "language" : "de",
    "shortcut" : "Amort",
    "description" : "Amortisation direkt",
    "sortNr" : 10
  }, {
    "code" : "2000",
    "language" : "fr",
    "shortcut" : "Amort",
    "description" : "Amortissement direct",
    "sortNr" : 10
  }, {
    "code" : "2000",
    "language" : "en",
    "shortcut" : "Amort",
    "description" : "Amortisation direct",
    "sortNr" : 10
  }, {
    "code" : "2001",
    "language" : "de",
    "shortcut" : "Amort",
    "description" : "Amortisation indirekt Versicherung",
    "sortNr" : 30
  }, {
    "code" : "2001",
    "language" : "fr",
    "shortcut" : "Amort",
    "description" : "Amortissement indirect assurances",
    "sortNr" : 30
  }, {
    "code" : "2001",
    "language" : "en",
    "shortcut" : "Amort",
    "description" : "Amortisation indirect insurance",
    "sortNr" : 30
  }, {
    "code" : "2002",
    "language" : "de",
    "shortcut" : "Amort",
    "description" : "Amortisation indirekt via Konto/Portfolio",
    "sortNr" : 20
  }, {
    "code" : "2002",
    "language" : "fr",
    "shortcut" : "Amort",
    "description" : "Amortissement indirect",
    "sortNr" : 20
  }, {
    "code" : "2002",
    "language" : "en",
    "shortcut" : "Amort",
    "description" : "Amortisation indirect",
    "sortNr" : 20
  }, {
    "code" : "2009",
    "language" : "de",
    "shortcut" : "VerfF",
    "description" : "Verfall Festhypotheken",
    "sortNr" : 35
  }, {
    "code" : "2009",
    "language" : "fr",
    "shortcut" : "EchHy",
    "description" : "Echéance d'hypothèques fixes",
    "sortNr" : 35
  }, {
    "code" : "2009",
    "language" : "en",
    "shortcut" : "Expy",
    "description" : "Expiry fixed-rate mortgage",
    "sortNr" : 35
  }, {
    "code" : "2010",
    "language" : "de",
    "shortcut" : "AmSal",
    "description" : "Amortisation zur Saldierung",
    "sortNr" : 60
  }, {
    "code" : "2010",
    "language" : "fr",
    "shortcut" : "AmPCl",
    "description" : "Amortissement pour clôture",
    "sortNr" : 60
  }, {
    "code" : "2010",
    "language" : "en",
    "shortcut" : "AmBal",
    "description" : "Amortisation on balancing",
    "sortNr" : 60
  }, {
    "code" : "3000",
    "language" : "de",
    "shortcut" : "Annui",
    "description" : "Annuität mit festem Endtermin",
    "sortNr" : 40
  }, {
    "code" : "3000",
    "language" : "fr",
    "shortcut" : "Annui",
    "description" : "Annuité - terme fixe",
    "sortNr" : 40
  }, {
    "code" : "3000",
    "language" : "en",
    "shortcut" : "Annui",
    "description" : "Annuity with fixed expiry date",
    "sortNr" : 40
  }, {
    "code" : "3001",
    "language" : "de",
    "shortcut" : "Annui",
    "description" : "Annuität mit festen Raten",
    "sortNr" : 50
  }, {
    "code" : "3001",
    "language" : "fr",
    "shortcut" : "Annui",
    "description" : "Annuité à taux fixe",
    "sortNr" : 50
  }, {
    "code" : "3001",
    "language" : "en",
    "shortcut" : "Annui",
    "description" : "Annuity with fixed installments",
    "sortNr" : 50
  } ]
}