{
    "key": "",
    "id": "",
    "clientKey": "",
    "creditLineDesignation": null,
    "purpose": "",
    "use": "",
    "overallLimit": {
        "amount": null,
        "currency:currency": {
            "href": ""
        },
        "_links": {
            "curies": [
            ]
        }
    },
    "resubmission": "",
    "BusinessCases:businessCase": {
        "href": null
    },
    "LoanRequestReason:reasonForApplication": {
        "href": null
    },
    "_links": {
        "curies": [
        ]
    }
}