package ch.axonivy.fintech.mock.dis.rest.resources;

import static org.junit.Assert.*;

import java.net.URISyntaxException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.Is.is;

public class CgateMultiformTest {
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void normalizePortInUrl_with_null_url() throws URISyntaxException {
		thrown.expect(IllegalArgumentException.class);
		CgateMultiform.normalizePortInUrl(null);
	}
	
	@Test
	public void normalizePortInUrl_with_empty_url() throws URISyntaxException {
		thrown.expect(IllegalArgumentException.class);
		CgateMultiform.normalizePortInUrl("");
	}
	
	@Test
	public void normalizePortInUrl_with_invalid_url() throws URISyntaxException {
		thrown.expect(URISyntaxException.class);
		CgateMultiform.normalizePortInUrl("http://");
	}
	
	@Test
	public void normalizePortInUrl_with_ip_url() throws URISyntaxException {
		String url = "http://168.10.25.26:8080/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_ip_url_and_zero_port() throws URISyntaxException {
		String url = "http://168.10.25.26:0/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		String expectedResult = "http://168.10.25.26/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		assertThat(result, is(expectedResult));
	}
	
	@Test
	public void normalizePortInUrl_with_ip_url_and_no_port() throws URISyntaxException {
		String url = "http://168.10.25.26/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_localhost_url() throws URISyntaxException {
		String url = "http://localhost:8080/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_localhost_url_and_no_port() throws URISyntaxException {
		String url = "http://localhost/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_localhost_url_and_zero_port() throws URISyntaxException {
		String url = "http://localhost:0/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		String expectedResult = "http://localhost/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		assertThat(result, is(expectedResult));
	}
	
	@Test
	public void normalizePortInUrl_with_localhostName_url() throws URISyntaxException {
		String url = "http://mypcname:8080/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_localhostName_url_and_no_port() throws URISyntaxException {
		String url = "http://mypcname/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_localhostName_and_zero_port() throws URISyntaxException {
		String url = "http://mypcname:0/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		String expectedResult = "http://mypcname/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		assertThat(result, is(expectedResult));
	}
	
	@Test
	public void normalizePortInUrl_with_domain_url() throws URISyntaxException {
		String url = "http://fintech.axonivy.io/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_domain_url_and_port() throws URISyntaxException {
		String url = "http://fintech.axonivy.io:8080/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_domain_url_and_zero_port() throws URISyntaxException {
		String url = "http://fintech.axonivy.io:0/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		String expectedResult = "http://fintech.axonivy.io/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		assertThat(result, is(expectedResult));
	}
	
	@Test
	public void normalizePortInUrl_with_https_domain_url_and_port() throws URISyntaxException {
		String url = "https://fintech.axonivy.io:8080/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	
	@Test
	public void normalizePortInUrl_with_https_domain_url_and_zero_port() throws URISyntaxException {
		String url = "https://fintech.axonivy.io:0/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		String expectedResult = "https://fintech.axonivy.io/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		assertThat(result, is(expectedResult));
	}
	
	@Test
	public void normalizePortInUrl_no_protocol_set() throws URISyntaxException {
		String url = "fintech.axonivy.io/ivy/pro/designer/services_mock/162D9B06E7844FCB/videoIdentification.ivp?gender=Herr&firstname=Emmanuel&lastname=M%FCller";
		String result = CgateMultiform.normalizePortInUrl(url);
		
		assertThat(result, is(url));
	}
	

}
